import {Component, Inject, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Rate} from 'app/bussiness/model/rate.model';

@Component({
  selector: 'fuse-review-form-dialog',
  templateUrl: './review-form.component.html',
  styleUrls: ['./review-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class FuseReviewFormDialogComponent {
  reviewTitle: string;
  reviewForm: FormGroup;
  action: string;
  review: Rate;
  avatar: string;

  constructor(
    public dialogRef: MatDialogRef<FuseReviewFormDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private formBuilder: FormBuilder,
  ) {
    this.action = data.action;
    if (this.action === 'edit') {
      this.reviewTitle = 'Edit Review';
      this.review = data.review;
    } else {
      this.reviewTitle = 'New Review';
      this.review = new Rate({});
    }

    this.reviewForm = this.createReviewForm();
  }

  createReviewForm(): FormGroup {
    return this.formBuilder.group({
      _id: [this.review._id],
      name: [this.review.name],
      date: [this.review.date],
      rate: [this.review.rate],
      review: [this.review.review],
      disabled: this.review.disabled !== undefined ? [this.review.disabled] : true,
    });
  }
}
