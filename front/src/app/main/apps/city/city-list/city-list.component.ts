import {Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation, Input} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {MatDialog, MatDialogRef, MatSort} from '@angular/material';
import {DataSource} from '@angular/cdk/collections';
import {merge, Observable, Subject} from 'rxjs';
import {map, takeUntil} from 'rxjs/operators';
import {environment} from '../../../../../environments/environment';
import {fuseAnimations} from '@fuse/animations';
import {FuseConfirmDialogComponent} from '@fuse/components/confirm-dialog/confirm-dialog.component';
 
import {CityModuleService} from 'app/main/apps/city/city.module.service';
import {CitiesCityFormDialogComponent} from 'app/main/apps/city/forms/city-form/city-form.component';
import {City} from 'app/bussiness/model/city.model';
import {Country} from 'app/bussiness/model/country.model';
import {Region} from 'app/bussiness/model/region.model';

@Component({
    selector: 'city-contact-list',
    templateUrl: './city-list.component.html',
    styleUrls: ['./city-list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class CitiesCityListComponent implements OnInit, OnDestroy {
    @ViewChild('dialogContent')
    dialogContent: TemplateRef<any>;
    @ViewChild(MatSort) sort: MatSort;

    @Input()
    set region(val: Region) {
        this._citiesService.onFilterChanged.next(val);
    }

    cities: any;
    countries: Country[];
    regions: Region[];
    apiHost: string = environment.apihost;
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'name_heb', 'name_en', 'region', 'country', 'buttons'];
    selectedContacts: any[];
    checkboxes: {};
    dialogRef: any;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ContactsService} _contactsService
     * @param {MatDialog} _matDialog
     */

    constructor(
        private _citiesService: CityModuleService,
        public _matDialog: MatDialog
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.dataSource = new FilesDataSource(this._citiesService, this.sort);
        this._citiesService.onCitiesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(cities => {
                this.cities = cities;
                this.checkboxes = {};
                cities.map(city => {
                    this.checkboxes[city._id] = false;
                });
            });
        this._citiesService.onRegionsChanged.subscribe(data => {
            this.regions = data;
        });
        this._citiesService.onCountriesChanged.subscribe(data => {
            this.countries = data;
        });
        /*this._citiesService.onSelectedCityChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(selectedContacts => {
                for ( const id in this.checkboxes )
                {
                    if ( !this.checkboxes.hasOwnProperty(id) )
                    {
                        continue;
                    }

                    this.checkboxes[id] = selectedContacts.includes(id);
                }
                this.selectedContacts = selectedContacts;
            });*/

        /*this._contactsService.onUserDataChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(user => {
                this.user = user;
            });*/

        /*this._contactsService.onFilterChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this._contactsService.deselectContacts();
            });*/
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Edit contact
     *
     * @param contact
     */
    editContact(city): void {
        this.dialogRef = this._matDialog.open(CitiesCityFormDialogComponent, {
            panelClass: 'contact-form-dialog',
            data: {
                city: city,
                countries: this.countries,
                regions: this.regions,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        const newCity: City = new City({});
                        newCity.name.en = formData.value.name_en;
                        newCity.name.heb = formData.value.name_heb;
                        newCity.description.en = formData.value.description_en;
                        newCity.description.heb = formData.value.description_heb;
                        newCity.country = formData.value.country;
                        newCity.pic = formData.value.pic;
                        newCity.inPopular = formData.value.inPopular;
                        newCity.inReview = formData.value.inReview;
                        newCity.region = formData.value.region;
                        newCity.seo = formData.value.seo;
                        this._citiesService.updateCity(city._id, newCity);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteCity(city);

                        break;
                }
            });
    }

    /*/!**
     * Delete City
     *!/*/
    deleteCity(city): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this._citiesService.deleteCity(city);
            }
            this.confirmDialogRef = null;
        });

    }

    /*
        /**
         * On selected change
         *
         * @param contactId
         */
    /*onSelectedChange(contactId): void
    {
        this._contactsService.toggleSelectedContact(contactId);
    }*/

    /**
     * Toggle star
     *
     * @param contactId
     */
    /*toggleStar(contactId): void
    {
        if ( this.user.starred.includes(contactId) )
        {
            this.user.starred.splice(this.user.starred.indexOf(contactId), 1);
        }
        else
        {
            this.user.starred.push(contactId);
        }

        this._contactsService.updateUserData(this.user);
    }*/
}

export class FilesDataSource extends DataSource<any> {
    /**
     * Constructor
     *
     * @param {CityModuleService} _citiesService
     */
    constructor(
        private _citiesService: CityModuleService,
        private _matSort: MatSort
    ) {
        super();
    }

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            this._citiesService.onCitiesChanged,
            // this._matPaginator.page,
            this._citiesService.onFilterChanged,
            this._matSort.sortChange
        ];

        return merge(...displayDataChanges).pipe(
            map(() => {
                let data = this._citiesService.cities.slice();
                data = data.filter(item => this._citiesService.region ? this._citiesService.region._id === item.region : true);
                data = this.sortData(data);
                return data;
            })
        );
    }

    /**
     * Sort data
     *
     * @param data
     * @returns {any[]}
     */
    sortData(data): any[] {
        if (!this._matSort.active || this._matSort.direction === '') {
            return data;
        }

        return data.sort((a, b) => {
            let propertyA: number | string = '';
            let propertyB: number | string = '';
            switch (this._matSort.active) {
                case 'name_en':
                    [propertyA, propertyB] = [a.name.en, b.name.en];
                    break;
                case 'name_heb':
                    [propertyA, propertyB] = [a.name.heb, b.name.heb];
                    break;
                case 'country':
                    [propertyA, propertyB] = [a.country, b.country];
                    break;
                case 'region':
                    [propertyA, propertyB] = [a.region, b.region];
                    break;
            }

            const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

            return (valueA < valueB ? -1 : 1) * (this._matSort.direction === 'asc' ? 1 : -1);
        });
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}
