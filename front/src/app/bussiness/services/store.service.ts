import {Injectable, EventEmitter} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from './base/service.base';
import {environment} from '../../../environments/environment';
import { IStore } from '../model/interfaces/IStore';
import {Observable} from 'rxjs';
import {IOrder} from '../model/interfaces/IOrder';
import {IMessage} from '../model/interfaces/IMessage';

@Injectable()
export class StoreService extends BaseService<IStore> {
    constructor(private http: HttpClient) {
        super(http, environment.apihost, environment.storeEndpoint);
    }

    public getBetweenDate (from:any,to:any) : Observable<IStore[]> {

        return this.httpClient.get(`${this.url}${this.endpoint}/get-by-date/${from}/${to}`).map((data: any) => {
            return data as IStore[];
        });
    }
    public getNameAndId () : Observable<any> {
        return this.httpClient.get(`${this.url}${this.endpoint}/get-id-name/`).map((data: any) => {
            return data ;
        });
    }


}
