import {ITranslate} from './ITranslate';
import {ICategory} from './ICategory';
import {IRate} from './IRate';

export interface IItem {
    _id?: string;
    name: ITranslate;
    code: string;
    picture: string;
    description: ITranslate;
    additional: boolean;
    recommended: boolean;
    inStock: boolean;
    rate: Array<IRate>;
    category: Array<string> | Array<ICategory>;
    dayDeal: boolean;
    price: number;
}
