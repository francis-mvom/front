import {Component, ViewEncapsulation} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

import {MatDialog} from '@angular/material';

import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {fuseAnimations} from '@fuse/animations';

import {CityModuleService} from './city.module.service';

import {locale as english} from './i18n/en';
import {locale as turkish} from './i18n/tr';
import {City} from '../../../bussiness/model/city.model';
import {Country} from '../../../bussiness/model/country.model';
import {Region} from '../../../bussiness/model/region.model';
import {CountriesCountryFormDialogComponent} from './forms/country-form/country-form.component';
import {RegionsRegionFormDialogComponent} from './forms/region-form/region-form.component';
import {CitiesCityFormDialogComponent} from './forms/city-form/city-form.component';

@Component({
    selector: 'city',
    templateUrl: './city.component.html',
    styleUrls: ['./city.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class CityComponent {
    dialogRef: any;
    hasSelectedContacts: boolean;
    searchInput: FormControl;
    cities: Array<City>;
    countries: Array<Country>;
    regions: Array<Region>;

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private cityModuleService: CityModuleService,
        private _fuseSidebarService: FuseSidebarService,
        private _matDialog: MatDialog
    ) {
        this._fuseTranslationLoaderService.loadTranslations(english, turkish);
        this.cityModuleService.onCitiesChanged.subscribe(data => {
            this.cities = data;
        });
        this.cityModuleService.onCountriesChanged.subscribe(data => {
            this.countries = data;
        });
        this.cityModuleService.onRegionsChanged.subscribe(data => {
            this.regions = data;
        });
    }

    editCountry(clientValue: Country): void {
        this.dialogRef = this._matDialog.open(CountriesCountryFormDialogComponent, {
            panelClass: 'contact-form-dialog',
            data: {
                action: 'edit',
                country: clientValue
            }
        });
        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                const newCountry: Country = new Country({});
                newCountry._id = clientValue._id;
                newCountry.name.en = response[1].value.name_en;
                newCountry.name.heb = response[1].value.name_heb;
                this.cityModuleService.updateCountry(clientValue._id, newCountry);
            });
    }

    deleteCountry(clientValue: Country): void {
        this.cityModuleService.deleteCountry(clientValue);
    }

    editRegion(clientValue: Region): void {
        this.dialogRef = this._matDialog.open(RegionsRegionFormDialogComponent, {
            panelClass: 'contact-form-dialog',
            data: {
                action: 'edit',
                region: clientValue,
                countries: this.countries,
            }
        });
        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                const newRegion: Region = new Region({});
                newRegion._id = clientValue._id;
                newRegion.name.en = response[1].value.name_en;
                newRegion.name.heb = response[1].value.name_heb;
                newRegion.country = response[1].value.country;
                newRegion.pic = response[1].value.pic === '' ||
                    response[1].value.pic === undefined ||
                    response[1].value.pic === null ? 'no-photo-available.png' : response[1].value.pic;
                this.cityModuleService.updateRegion(clientValue._id, newRegion);
            });
    }

    public newCountry(): void {
        this.dialogRef = this._matDialog.open(CountriesCountryFormDialogComponent, {
            panelClass: 'contact-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                const newCountry: Country = new Country({});
                newCountry.name.en = response.value.name_en;
                newCountry.name.heb = response.value.name_heb;
                this.cityModuleService.updateCountry(null, newCountry);
            });
    }

    public newRegion(): void {
        this.dialogRef = this._matDialog.open(RegionsRegionFormDialogComponent, {
            panelClass: 'contact-form-dialog',
            data: {
                countries: this.countries,
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                const newRegion: Region = new Region({});
                newRegion.name.en = response.value.name_en;
                newRegion.name.heb = response.value.name_heb;
                newRegion.country = response.value.country;
                newRegion.pic =  response.value.picture === '' ||
                response.value.picture === undefined ||
                response.value.pic === null ? 'no-photo-available.png' : response.value.pic;
                this.cityModuleService.updateRegion(null, newRegion);
            });
    }

    public newCity(): void {
        this.dialogRef = this._matDialog.open(CitiesCityFormDialogComponent, {
            panelClass: 'contact-form-dialog',
            data: {
                action: 'new',
                countries: this.countries,
                regions: this.regions
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                const newCity: City = new City({});
                newCity.name.en = response.value.name_en;
                newCity.name.heb = response.value.name_heb;
                newCity.description.en = response.value.description_en;
                newCity.description.heb = response.value.description_heb;
                newCity.country = response.value.country;
                newCity.region = response.value.region;
                newCity.pic = response.value.pic;
                newCity.inPopular = response.value.inPopular;
                newCity.inReview = response.value.inReview;
                newCity.seo = response.value.seo;
                this.cityModuleService.updateCity(null, newCity);
            });
    }

    deleteRegion(region: Region): void{
        this.cityModuleService.deleteRegion(region);
    }

    /**
     * Toggle the sidebar
     *
     * @param name
     */
    toggleSidebar(name): void
    {
        this._fuseSidebarService.getSidebar(name).toggleOpen();
    }
}
