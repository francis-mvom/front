import {Injectable, EventEmitter} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from './base/service.base';
import {environment} from '../../../environments/environment';

import { IMessage } from '../model/interfaces/IMessage';
import { Observable } from 'rxjs';

@Injectable()
export class MessageService extends BaseService<IMessage> {
    constructor(private http: HttpClient) {
        super(http, environment.apihost, environment.messageEndpoint);
    }

   public getBetweenDate (from:any,to:any) : Observable<IMessage[]> {
       
    return this.httpClient.get(`${this.url}${this.endpoint}/get-by-date/${from}/${to}`).map((data: any) => {
        return data as IMessage[];
    });
   }
   public getByType (type:any) : Observable<IMessage[]> {
       
    return this.httpClient.get(`${this.url}${this.endpoint}/get-by-type/${type}`).map((data: any) => {
        return data as IMessage[];
    });
   }
}