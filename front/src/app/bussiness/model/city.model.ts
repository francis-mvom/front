import {FuseUtils} from '@fuse/utils';
import {ITranslate} from './interfaces/ITranslate';
import { ICity } from './interfaces/ICity';
import {ICountry} from './interfaces/ICountry';
import {IRegion} from './interfaces/IRegion';

export class City implements ICity {
    _id: string;
    name: ITranslate;
    description: ITranslate;
    pic: string;
    region:  IRegion | string;
    country: ICountry | string;
    inPopular: boolean;
    inReview: boolean;
    itemCount: number;
    averagePrice: number;
    seo: any;
    /**
     * Constructor
     *
     * @param item
     */
    constructor(item) {
        {
            this._id = item._id || '';
            this.name = item.name || {heb: '', en: ''};
            this.description = item.description || {heb: '', en: ''};
            this.pic = (item.pic === '' || item.pic === undefined) ? '' : item.pic;
            this.country = item.country || '';
            this.region = item.region || '';
            this.inPopular = item.inPopular;
            this.inReview = item.inReview;
            this.itemCount = item.itemCount || 0;
            this.averagePrice = item.averagePrice || 0;
            this.seo = item.seo;
        }
    }
}
