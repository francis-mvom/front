import {User} from '../../model/user.model';
import {AuthenticationActionTypes, AuthenticationActions} from '../actions/authentication.actions';

export interface State {
    isAuthenticated: boolean;
    user: any | null;
    errorMessage: string | null;
}

//set the initial state with localStorage
export const initialState: State = {
    isAuthenticated: localStorage.getItem('token') !== null,
    user: {
        token: localStorage.getItem('token'),
        user: localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')):null
    },
    errorMessage: null
};

export function reducer(state = initialState, action: AuthenticationActions): State {

    switch (action.type) {
        case AuthenticationActionTypes.LOGIN_SUCCESS: {
            console.log(action.payload.response);
            return {
                ...state,
                isAuthenticated: true,
                user: {
                    token: action.payload.response.token,
                    user: action.payload.response.user
                },
                errorMessage: null
            };
        }
        case AuthenticationActionTypes.LOGIN_FAILURE: {
            return {
                ...state,
                errorMessage: 'Wrong credentials.'
            };
        }
        case AuthenticationActionTypes.LOGOUT: {
            return initialState;
        }
        default: {
            return state;
        }
    }
}