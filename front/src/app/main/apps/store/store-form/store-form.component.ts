import { Component, Inject, ViewEncapsulation } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";

import { ICity } from "app/bussiness/model/interfaces/ICity";
import { Store } from "app/bussiness/model/store.model";

import { CityService } from "app/bussiness/services/city.service";
import { environment } from "environments/environment";

@Component({
    selector: "fuse-stores-store-form-dialog",
    templateUrl: "./store-form.component.html",
    styleUrls: ["./store-form.component.scss"],
    encapsulation: ViewEncapsulation.None
})
export class FuseStoresStoreFormDialogComponent {
    dialogTitle: string;
    storeForm: FormGroup;
    action: string;
    store: Store;
    avatar: string;

    // driverAvatarURL: string = environment.serverUrl + 'assets/images/drivers/';

    constructor(
        public dialogRef: MatDialogRef<FuseStoresStoreFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private cityService: CityService
    ) {
        this.action = data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit store';
            this.store = data.store;
        } else {
            this.dialogTitle = 'New Store';
            this.store = new Store({});
        }

        this.cityService.list().subscribe(data => {
            // this.cities = data;
        });

        this.storeForm = this.createStoreForm();
    }

    createStoreForm(): FormGroup {
        return this.formBuilder.group({
            _id: [this.store._id],
            name_heb: [this.store.name.heb],
            name_en: [this.store.name.en],
            description_heb: [this.store.description.heb],
            description_en: [this.store.description.en],
            address_en: [this.store.address.en],
            address_heb: [this.store.address.heb],
            tel: [this.store.tel],
            fax: [this.store.fax],
            mobile: [this.store.mobile],
            mail: [this.store.mail],
        });
    }
}
