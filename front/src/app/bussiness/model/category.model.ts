import {ITranslate} from './interfaces/ITranslate';
import {ICategory} from './interfaces/ICategory';
import {IItem} from './interfaces/IItem';
import {Seo} from './seo';

export class Category implements ICategory {
    _id?: string;
    name: ITranslate;
    tagline: ITranslate;
    description: ITranslate;
    pic: string;
    count: number;
    renderOnHomePage: boolean;
    disabled: boolean;
    toTopMenu: boolean;
    toFooterMenu: boolean;
    subcategory: Array<string> | Array<Category>;
    avgPrice: number;
    items: Array<IItem> | Array<string>;
    seo: Seo;
    /**
     * Constructor
     *
     * @param category
     */
    constructor(item) {
        {
            if(item._id)
                this._id = item._id;
            this.name = item.name || {heb: '', en: ''};
            this.description = item.description || {heb: '', en: ''};
            this.pic = (item.pic === '' || item.pic === undefined) ? '' : item.pic;
            this.tagline = item.tagline || {heb: '', en: ''};
            this.count = item.count || 0;
            this.avgPrice = item.avgPrice || 0;
            this.items = item.items || [];
            this.renderOnHomePage = item.renderOnHomePage;
            this.disabled = item.disabled;
            this.toFooterMenu = item.toFooterMenu;
            this.toTopMenu = item.toTopMenu;
            this.subcategory = item.subcategory;
            this.seo = item.seo;
        }
    }
}
