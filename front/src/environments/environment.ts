// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    // apihost: 'http://188.120.225.152:3000',
    // apihost: 'http://localhost:3000',
    apihost: 'https://backend.hashve.co.il',
    userEndpoint: '/api/v1/user',
    cityEndpoint: '/api/v1/city',
    countryEndpoint: '/api/v1/country',
    regionEndpoint: '/api/v1/region',
    itemEndpoint: '/api/v1/item',
    categoryEndpoint: '/api/v1/category',
    storeEndpoint: '/api/v1/store',
    packageEndpoint: '/api/v1/package',
    orderEndpoint: '/api/v1/order',
    articleEndpoint: '/api/v1/article',
    messageEndpoint: '/api/v1/message',
    uploadFile: '/api/v1/upload',
    uploadCategory: '/category',
    uploadItem: '/item',
    uploadRegion: '/region',
    uploadCity: '/city',
    uploadStore: '/store',
    uploadArticle: '/articles',
    hmr       : false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
