import {ITranslate} from './ITranslate';
import {ICountry} from './ICountry';
import {IRegion} from './IRegion';

export interface ICity {
    _id?: string;
    name: ITranslate;
    pic: string;
    inPopular: boolean;
    inReview: boolean;
    region: IRegion | string;
    country: ICountry | string;
    itemCount: number;
    averagePrice: number;
    seo: any;
}