// import {ITranslate} from './interfaces/ITranslate';
import { IRate } from './interfaces/IRate';

export class Rate implements IRate {
    _id: string;
    date: Date;
    rate: number;
    review: string;
    name: string;
    disabled: boolean;
    /**
     * Constructor
     *
     * @param rate
     */
    constructor(rate) {
        {
            this._id = rate._id || '';
            this.name = rate.name || '';
            this.date = rate.date || new Date();
            this.rate = rate.rate || 0;
            this.review = rate.review || '';
            this.disabled = rate.disabled || false;
        }
    }
}
