import {Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {MatDialog, MatDialogRef,MatSort} from '@angular/material';
import {DataSource} from '@angular/cdk/collections';
import {merge, Observable, Subscription} from 'rxjs';

import {fuseAnimations} from '@fuse/animations';
import {FuseConfirmDialogComponent} from '@fuse/components/confirm-dialog/confirm-dialog.component';
import {environment} from 'environments/environment';
import {StoreModuleService} from 'app/main/apps/store/store.module.service';
import {map} from 'rxjs/operators';

@Component({
    selector: 'fuse-stores-store-list',
    templateUrl: './store-list.component.html',
    styleUrls: ['./store-list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class FuseStoresStoreListComponent implements OnInit, OnDestroy {
    @ViewChild('dialogContent') dialogContent: TemplateRef<any>;
    @ViewChild(MatSort) sort: MatSort;

    stores: any;
    user: any;
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'logo', 'name', 'phone', 'fax', 'mobile', 'mail', 'buttons'];
    selectedStores: any[];
    checkboxes: {};

    serverPath: string;
    driverAvatar: string;

    onStoresChangedSubscription: Subscription;
    onFilterChangedSubscription: Subscription;
    onSelectedStoresChangedSubscription: Subscription;

    dialogRef: any;

    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    constructor(
        private storeModuleService: StoreModuleService,
        public dialog: MatDialog
    ) {
        this.serverPath = environment.apihost;
        this.onStoresChangedSubscription =
            this.storeModuleService.onStoresChanged.subscribe(stores => {

                this.stores = stores;
                this.checkboxes = {};
                stores.map(store => {
                    this.checkboxes[store._id] = false;
                });
            });

        this.onSelectedStoresChangedSubscription =
            this.storeModuleService.onSelectedStoresChanged.subscribe(selectedStores => {
                for (const id in this.checkboxes) {
                    if (!this.checkboxes.hasOwnProperty(id)) {
                        continue;
                    }

                    this.checkboxes[id] = selectedStores.includes(id);
                }
                this.selectedStores = selectedStores;
            });

        this.onFilterChangedSubscription =
            this.storeModuleService.onFilterChanged.subscribe(() => {
                this.storeModuleService.deselectStores();
            });
    }

    onSelectedChange(storeID) {
        this.storeModuleService.toggleSelectedStore(storeID);
    }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this.storeModuleService,this.sort);
    }

    ngOnDestroy() {
        this.onStoresChangedSubscription.unsubscribe();
        this.onFilterChangedSubscription.unsubscribe();
        this.onSelectedStoresChangedSubscription.unsubscribe();
    }

    /**
     * Delete Contact
     */
    deleteStore(store) {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Delete the store?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.storeModuleService.deleteStore(store)/*.then(data => {
                    this.storeModuleService.onSearchTextChanged.next('');
                });*/
            }
            this.confirmDialogRef = null;
        });

    }
}

export class FilesDataSource extends DataSource<any> {
    constructor(
        private storeModuleService: StoreModuleService,
        private _matSort: MatSort
    ) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */


    connect(): Observable<any[]> {
        const displayDataChanges = [
            this.storeModuleService.onStoresChanged,
            this._matSort.sortChange,
            this.storeModuleService.onFilterChanged
        ];
        return merge(...displayDataChanges).pipe(
            map(() => {
                let data = this.storeModuleService.stores;

                const fiType = this.storeModuleService.filterBy;
                data = data.filter(item =>fiType.length===0 ? true : fiType.includes(item._id));
                data = this.sortData(data);

                return data;
            })
        );
    }
    disconnect() {
    }

    sortData(data): any[] {
        if (!this._matSort.active || this._matSort.direction === '') {
            return data;
        }
        return data.sort((a, b) => {
            let propertyA: number | string = '';
            let propertyB: number | string = '';
            switch (this._matSort.active) {
                case 'name':
                    [propertyA, propertyB] = [a.name.en, b.name.en];
                    break;
                case 'tel':
                    [propertyA, propertyB] = [a.tel, b.tel];
                    break;
                case 'fax':
                    [propertyA, propertyB] = [a.fax, b.fax];
                    break;
                case 'mobile':
                    [propertyA, propertyB] = [a.mobile, b.mobile];
                    break;
                case 'mail':
                    [propertyA, propertyB] = [a.mail, b.mail];
                    break;

            }

            const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

            return (valueA < valueB ? -1 : 1) * (this._matSort.direction === 'asc' ? 1 : -1);
        });
    }
}
