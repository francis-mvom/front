import { Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import {environment} from '../../../../../environments/environment';
import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import {ItemSize} from '../../../../bussiness/model/Enum/ItemSize';
import { PackageModuleService} from 'app/main/apps/package/package.module.service';
import { PackagesPackageFormDialogComponent } from 'app/main/apps/package/package-form/package-form.component';
import {Item} from '../../../../bussiness/model/item.model';

@Component({
    selector     : 'packages-package-list',
    templateUrl  : './package-list.component.html',
    styleUrls    : ['./package-list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class PackagesPackageListComponent implements OnInit, OnDestroy
{
    @ViewChild('dialogContent')
    dialogContent: TemplateRef<any>;
    ItemSize = ItemSize;
    packages: any;
    items: Array<Item> = [];
    user: any;
    apiHost: string = environment.apihost;
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'color', 'Name', 'ItemSize', 'Price', 'buttons'];
    selectedPackages: any[];
    checkboxes: {};
    dialogRef: any;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ContactsService} _contactsService
     * @param {MatDialog} _matDialog
     */
    constructor(
        private _packageModuleService: PackageModuleService,
        public _matDialog: MatDialog
    )
    {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this.dataSource = new FilesDataSource(this._packageModuleService);

        this._packageModuleService.onItemDataChanged.subscribe(data => {
            this.items = data;
        })
 
        this._packageModuleService.onPackageChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(packages => {
                this.packages = packages;

                this.checkboxes = {};
                packages.map(pack => {
                    this.checkboxes[pack._id] = false;
                });
            });

        this._packageModuleService.onSelectedPackagesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(selectedPackages => {
                for ( const id in this.checkboxes )
                {
                    if ( !this.checkboxes.hasOwnProperty(id) )
                    {
                        continue;
                    }

                    this.checkboxes[id] = selectedPackages.includes(id);
                }
                this.selectedPackages = selectedPackages;
            });

        /*this._packageModuleService.onUserDataChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(user => {
                this.user = user;
            });*/

        this._packageModuleService.onFilterChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this._packageModuleService.deselectPackages();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Edit contact
     *
     * @param contact
     */
    editPackage(pack): void
    {
        this.dialogRef = this._matDialog.open(PackagesPackageFormDialogComponent, {
            panelClass: 'contact-form-dialog',
            data      : {
                package: pack,
                items: this.items,
                action : 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch ( actionType )
                {
                    /**
                     * Save
                     */
                    case 'save':

                        this._packageModuleService.updatePackage(pack._id, formData.getRawValue());

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deletePackage(pack);

                        break;
                }
            });
    }

    /**
     * Delete Contact
     */
    deletePackage(pack): void
    {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this._packageModuleService.deletePackage(pack);
            }
            this.confirmDialogRef = null;
        });

    }

    /**
     * On selected change
     *
     * @param packageId
     */
    onSelectedChange(packageId): void
    {
        this._packageModuleService.toggleSelectedPackages(packageId);
    }

}

export class FilesDataSource extends DataSource<any>
{
    /**
     * Constructor
     *
     * @param {ContactsService} _contactsService
     */
    constructor(
        private _packageModuleService: PackageModuleService
    )
    {
        super();
    }

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]>
    {
        return this._packageModuleService.onPackageChanged;
    }

    /**
     * Disconnect
     */
    disconnect(): void
    {
    }
}
