import {Component, Inject, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, FormControl} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {environment} from '../../../../../../environments/environment';
import {Category} from '../../../../../bussiness/model/category.model';
import {FileUploadService} from '../../../../../bussiness/services/file-upload.service';
import {AngularEditorConfig} from '@kolkov/angular-editor';
import {CategoryService} from '../../../../../bussiness/services/category.service';
import {Observable, Subscription} from 'rxjs';
import {FileUploader} from 'ng2-file-upload';

@Component({
    selector: 'category-contact-form-dialog',
    templateUrl: './category-form.component.html',
    styleUrls: ['./category-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class CategoriesCategoryFormDialogComponent {
    action: string;
    category: Category;
    categoryForm: FormGroup;
    categories: Observable<Array<Category>>;
    dialogTitle: string;
    apiHost: string = environment.apihost;
    categoryPicPath: string = '/assets/category/';
    // picPath: string = environment.apihost + environment.uploadFile + environment.uploadCategory;
    public uploader: FileUploader = new FileUploader({ url: environment.apihost + environment.uploadFile + environment.uploadCategory, itemAlias: 'file' });
    editorConfig: AngularEditorConfig = {
        editable: true,
        spellcheck: true,
        height: '25rem',
        minHeight: '5rem',
        placeholder: 'Enter text here...',
        translate: 'no',
        uploadUrl: 'v1/images', // if needed
        customClasses: [ // optional
            {
                name: "quote",
                class: "quote",
            },
            {
                name: 'redText',
                class: 'redText'
            },
            {
                name: "titleText",
                class: "titleText",
                tag: "h1",
            },
        ]
    };
    /**
     * Constructor
     *
     * @param {MatDialogRef<CitiesCityFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<CategoriesCategoryFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder,
        private fileUploadService: FileUploadService,
        private categoryService: CategoryService
    ) {
        // Set the defaults
        this.action = _data.action;
        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Category';
            this.category = _data.category;
        } else {
            this.dialogTitle = 'New Category';
            this.category = new Category({});
        }

        this.categoryForm = this.createContactForm();
        /*this.fileUploadService.onPhotoUploaded.subscribe(data => {
            this.categoryForm.get('pic').setValue(data);
        });*/

        this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            // console.log('FileUpload:uploaded successfully:', item, status, response);
            this.categoryForm.get('pic').setValue(JSON.parse(response).data[0].name);
            alert('Your file has been uploaded successfully');
        };

        this.categories = this.categoryService.list().map(data => {
            data.sort((a, b) => {
                return a.name.heb < b.name.heb ? -1 : 1;
            });
            return data;
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create contact form
     *
     * @returns {FormGroup}
     */
    createContactForm(): FormGroup {
        console.log(this.category);
        return this._formBuilder.group({
            _id: [this.category._id],
            name_en: [this.category.name.en],
            name_heb: [this.category.name.heb],
            tagline_en: [this.category.tagline.en],
            tagline_heb: [this.category.tagline.heb],
            description_en: [this.category.description.en],
            description_heb: [this.category.description.heb],
            pic: [this.category.pic],
            renderOnHomePage: [this.category.renderOnHomePage !== undefined?this.category.renderOnHomePage:false],
            disabled: [this.category.disabled !== undefined?this.category.disabled:true],
            toTopMenu: [this.category.toTopMenu !== undefined?this.category.toTopMenu:false],
            toFooterMenu: [this.category.toFooterMenu !== undefined?this.category.toFooterMenu:false],
            subcategory: [this.category.subcategory],
            seo: new FormControl(this.category.seo)
        });
    }
}
