import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes,CanActivate,CanActivateChild } from '@angular/router';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule, MatIconModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';

import { fuseConfig } from 'app/fuse-config';

import { AppComponent } from 'app/app.component';
import { LayoutModule } from 'app/layout/layout.module';
import { SampleModule } from 'app/main/sample/sample.module';
import {UserService} from 'app/bussiness/services/user.service';
import {CategoryService} from 'app/bussiness/services/category.service';
import {CityService} from 'app/bussiness/services/city.service';
import {CountryService} from 'app/bussiness/services/country.service';
import {RegionService} from 'app/bussiness/services/region.service';
import {ItemService} from 'app/bussiness/services/item.service';
import {FileUploadService} from 'app/bussiness/services/file-upload.service';
import {PackageService} from 'app/bussiness/services/package.service';
import {StoreService} from 'app/bussiness/services/store.service';
import {OrderService} from 'app/bussiness/services/order.service';
import {HashveModule} from 'app/components/hashve.module';
import { MessageService } from './bussiness/services/message.service';
import {AuthenticationService} from './bussiness/services/authentication.service';
import {EffectsModule} from '@ngrx/effects';
import {AuthEffects} from './bussiness/store/effects/auth.effects';
import {StoreModule} from '@ngrx/store';
import {reducers} from './bussiness/store/app.state';
import {ErrorInterceptor, TokenInterceptor} from './bussiness/services/token.interceptor';
import {AuthGuardService} from './bussiness/services/auth-guard.service';



const appRoutes: Routes = [
    {
        path        : 'apps',
        canActivate: [AuthGuardService],
        loadChildren: './main/apps/apps.module#AppsModule'
    },
    {
        path      : 'login',
        loadChildren: './main/login/login.module#LoginModule'
    },
    {
        path      : '**',
        redirectTo: 'login'
    }
];

@NgModule({
    declarations: [
        AppComponent,

    ],
    imports     : [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes),
        EffectsModule.forRoot([AuthEffects]),
        StoreModule.forRoot(reducers, {}),
        HashveModule,
        TranslateModule.forRoot(),
        // Material moment date module
        MatMomentDateModule,

        // Material
        MatButtonModule,
        MatIconModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        // App modules
        LayoutModule,
        SampleModule
    ],
    providers: [
        UserService,
        CategoryService,
        CityService,
        RegionService,
        CountryService,
        ItemService,
        FileUploadService,
        PackageService,
        StoreService,
        OrderService,
        MessageService,
        AuthGuardService,
        AuthenticationService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorInterceptor,
            multi: true
        }
    ],
    exports: [

    ],
    bootstrap   : [
        AppComponent
    ]
})
export class AppModule
{
}
