import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CdkTableModule} from '@angular/cdk/table';
import {HashveModule} from 'app/components/hashve.module';
import {FuseReviewFormDialogComponent} from './review-form/review-form.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
// import {TdFileService, IUploadOptions} from '@covalent/core/file';
// import {PickListModule} from 'primeng/picklist';
import { FileUploadModule } from 'ng2-file-upload';

import {
    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSnackBarModule,
    MatSortModule,
} from '@angular/material';

import {FuseSharedModule} from '@fuse/shared.module';
import {FuseConfirmDialogModule} from '@fuse/components';

import {FuseStoreComponent} from './store.component';

import {StoreModuleService} from './store.module.service';
import {StoreComponentService} from './store/store.component.service';
import {FuseStoresStoreListComponent} from './store-list/store-list.component';
import {FuseStoreSelectedBarComponent} from './selected-bar/selected-bar.component';
import {FuseStoresStoreFormDialogComponent} from './store-form/store-form.component';

import {FuseStoreInfoComponent} from './store/store.info.component';

const routes: Routes = [
    {
      path: ':id',
      component: FuseStoreInfoComponent,
      resolve: {
        data: StoreComponentService
      }
    },
    {
      path: ':id/:handle',
      component: FuseStoreInfoComponent,
      resolve: {
        data: StoreComponentService
      }
    },
    {
        path: '**',
        component: FuseStoreComponent,
        resolve: {
            stores: StoreModuleService
        }
    },
];

@NgModule({
    declarations: [
        FuseStoreComponent,
        FuseStoreInfoComponent,
        FuseStoresStoreListComponent,
        FuseStoreSelectedBarComponent,
        FuseStoresStoreFormDialogComponent,
        FuseReviewFormDialogComponent,
    ],
    imports: [
        AngularEditorModule,
        RouterModule.forChild(routes),
        CdkTableModule,
        HashveModule,
        MatNativeDateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSelectModule,
        MatSidenavModule,
        MatTableModule,
        MatTabsModule,
        MatToolbarModule,
        MatRadioModule,
        MatSnackBarModule,
        FuseSharedModule,
        MatSortModule,
        FuseConfirmDialogModule,
        FileUploadModule
        // CovalentFileModule,
        // CovalentLoadingModule,
        // PickListModule
    ],
    providers: [
        // TdFileService,
        StoreModuleService,
        StoreComponentService
    ],
    entryComponents: [
        FuseStoresStoreFormDialogComponent,
        FuseReviewFormDialogComponent
    ]
})
export class StoreModule {
}
