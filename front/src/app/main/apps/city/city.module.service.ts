import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {
    ActivatedRouteSnapshot,
    Resolve,
    RouterStateSnapshot
} from '@angular/router';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {FuseUtils} from '@fuse/utils';

import {City} from 'app/bussiness/model/city.model';
import {Country} from 'app/bussiness/model/country.model';
import {Region} from 'app/bussiness/model/region.model';
import {CityService} from 'app/bussiness/services/city.service';
import {RegionService} from 'app/bussiness/services/region.service';
import {CountryService} from 'app/bussiness/services/country.service';

@Injectable()
export class CityModuleService implements Resolve<any> {
    onCitiesChanged: BehaviorSubject<any>;
    onRegionsChanged: BehaviorSubject<any>;
    onCountriesChanged: BehaviorSubject<any>;

    onSelectedContactsChanged: BehaviorSubject<any>;
    onUserDataChanged: BehaviorSubject<any>;
    onSearchTextChanged: Subject<any>;
    onFilterChanged: Subject<any>;

    cities: City[];
    regions: Region[];
    countries: Country[];

    user: any;
    selectedContacts: string[] = [];
    region: Region;
    searchText: string;
    filterBy: string;

    /**
     * Constructor
     *
     * @param {RegionService} regionService
     * @param {CityService} cityService
     * @param {CountryService} countryService
     */
    constructor(
        private regionService: RegionService,
        private cityService: CityService,
        private countryService: CountryService
    ) {
        // Set the defaults
        this.onCitiesChanged = new BehaviorSubject([]);
        this.onRegionsChanged = new BehaviorSubject([]);
        this.onCountriesChanged = new BehaviorSubject([]);

        this.onFilterChanged = new Subject();

        /*this.onSelectedContactsChanged = new BehaviorSubject([]);
        this.onUserDataChanged = new BehaviorSubject([]);
        this.onSearchTextChanged = new Subject();
        */
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {
            Promise.all([this.getCities(), this.getCountries(), this.getRegions()]).then(
                ([files]) => {
                    /*this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getContacts();
                    });*/
                    this.onFilterChanged.subscribe(filter => {
                        this.region = filter;
                    });
                    resolve(this.cities);
                }, reject);
        });
    }

    /**
     * Get cities
     *
     * @returns {Promise<any>}
     */
    getCities(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.cityService.list().subscribe(data => {
                this.cities = data.map(item => {
                    return new City(item);
                });
                this.onCitiesChanged.next(this.cities);
                resolve(data);
            });
        });
    }

    getCountries(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.countryService.list().subscribe(data => {
                this.countries = data.map(item => {
                    return new Country(item);
                });
                this.onCountriesChanged.next(this.countries);
                resolve(data);
            });
        });
    }

    getRegions(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.regionService.list().subscribe(data => {
                this.regions = data.map(item => {
                    return new Region(item);
                });
                this.onRegionsChanged.next(this.regions);
                resolve(data);
            });
        });
    }

    /**
     * Get user data
     *
     * @returns {Promise<any>}
     */
    /*getUserData(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient
                .get(
                    environment.apihost +
                        "/api/v1/user/5c92b722d1885128c8b0a81f"
                )
                .subscribe((response: any) => {
                    this.user = response;
                    this.onUserDataChanged.next(this.user);
                    resolve(this.user);
                }, reject);
        });
    }*/

    /**
     * Toggle selected contact by id
     *
     * @param id
     */
    /*toggleSelectedContact(id): void {
        // First, check if we already have that contact as selected...
        if (this.selectedContacts.length > 0) {
            const index = this.selectedContacts.indexOf(id);

            if (index !== -1) {
                this.selectedContacts.splice(index, 1);

                // Trigger the next event
                this.onSelectedContactsChanged.next(this.selectedContacts);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedContacts.push(id);

        // Trigger the next event
        this.onSelectedContactsChanged.next(this.selectedContacts);
    }*/

    /**
     * Toggle select all
     */
    /*toggleSelectAll(): void {
        if (this.selectedContacts.length > 0) {
            this.deselectContacts();
        } else {
            this.selectContacts();
        }
    }*/

    /**
     * Select contacts
     *
     * @param filterParameter
     * @param filterValue
     */

    /*selectContacts(filterParameter?, filterValue?): void {
        this.selectedContacts = [];

        // If there is no filter, select all contacts
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedContacts = [];
            this.contacts.map(contact => {
                this.selectedContacts.push(contact._id);
            });
        }

        // Trigger the next event
        this.onSelectedContactsChanged.next(this.selectedContacts);
    }*/

    /**
     * Update country
     *
     * @param contact
     * @returns {Promise<any>}
     */
    updateCountry(id, country): Promise<any> {
        if (id === null) {
            return new Promise((resolve, reject) => {
                this.countryService.create(country).subscribe(response => {
                    this.getCountries();
                    resolve(response);
                });
            });
        } else {
            country._id = id;
            return new Promise((resolve, reject) => {
                this.countryService.update(country).subscribe(response => {
                    this.getCountries();
                    resolve(response);
                });
            });
        }
    }

    updateRegion(id, region): Promise<any> {
        if (id === null) {
            return new Promise((resolve, reject) => {
                this.regionService.create(region).subscribe(response => {
                    this.getRegions();
                    resolve(response);
                });
            });
        } else {
            region._id = id;
            return new Promise((resolve, reject) => {
                this.regionService.update(region).subscribe(response => {
                    this.getRegions();
                    resolve(response);
                });
            });
        }
    }

    updateCity(id, city): Promise<any> {
        if (id === null) {
            return new Promise((resolve, reject) => {
                this.cityService.create(city).subscribe(response => {
                    this.getCities();
                    resolve(response);
                });
            });
        } else {
            city._id = id;
            return new Promise((resolve, reject) => {
                this.cityService.update(city).subscribe(response => {
                    this.getCities();
                    resolve(response);
                });
            });
        }
    }

    /**
     * Update user data
     *
     * @param userData
     * @returns {Promise<any>}
     */
    /*updateUserData(userData): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient
                .post("api/contacts-user/" + this.user.id, { ...userData })
                .subscribe(response => {
                    this.getUserData();
                    this.getContacts();
                    resolve(response);
                });
        });
    }*/

    /**
     * Deselect contacts
     */

    /*deselectContacts(): void {
        this.selectedContacts = [];

        // Trigger the next event
        this.onSelectedContactsChanged.next(this.selectedContacts);
    }*/

    /**
     * Delete country
     *
     * @param country
     */
    deleteCountry(country): void {
        this.countryService.delete(country._id).subscribe(data => {
            const contactIndex = this.countries.indexOf(country);
            this.countries.splice(contactIndex, 1);
            this.onCountriesChanged.next(this.countries);
        });
    }

    /**
     * Delete region
     *
     * @param region
     */
    deleteRegion(region): void {
        this.regionService.delete(region._id).subscribe(data => {
            const contactIndex = this.regions.indexOf(region);
            this.regions.splice(contactIndex, 1);
            this.onRegionsChanged.next(this.regions);
        });
    }

    /**
     * Delete city
     *
     * @param country
     */
    deleteCity(city): void {
        this.cityService.delete(city._id).subscribe(data => {
            const cityIndex = this.cities.indexOf(city);
            this.cities.splice(cityIndex, 1);
            this.onCitiesChanged.next(this.cities);
        });
    }

    /**
     * Delete selected contacts
     */
    /*deleteSelectedContacts(): void {
        for (const contactId of this.selectedContacts) {
            const contact = this.contacts.find(_contact => {
                return _contact._id === contactId;
            });
            const contactIndex = this.contacts.indexOf(contact);
            this.contacts.splice(contactIndex, 1);
            this.userService.delete(contact._id).subscribe(data => {});
        }
        this.onContactsChanged.next(this.contacts);
        this.deselectContacts();
    }*/
}
