import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LoginComponent} from './login.component';
import {RouterModule, Routes} from '@angular/router';
import {AuthEffects} from '../../bussiness/store/effects/auth.effects';
import {EffectsModule} from '@ngrx/effects';
import {AuthenticationService} from '../../bussiness/services/authentication.service';
import {StoreModule} from '@ngrx/store';
import {reducers} from '../../bussiness/store/app.state';
import {LoginModuleService} from './login.module.service';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {FuseSharedModule} from '../../../@fuse/shared.module';


const appRoutes: Routes = [
    {
        path      : '**',
        component: LoginComponent,
    }
];
@NgModule({
  declarations: [LoginComponent],
  imports: [
      CommonModule,
      EffectsModule.forRoot([AuthEffects]),
      StoreModule.forRoot(reducers, {}),
      RouterModule.forChild(appRoutes),
      MatButtonModule,
      MatCheckboxModule,
      MatFormFieldModule,
      MatIconModule,
      MatInputModule,
      FuseSharedModule
  ],
    providers: [LoginModuleService],
})
export class LoginModule { }
