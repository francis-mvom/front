import { TestBed } from '@angular/core/testing';

import { Login.ModuleService } from './login.module.service';

describe('Login.ModuleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Login.ModuleService = TestBed.get(Login.ModuleService);
    expect(service).toBeTruthy();
  });
});
