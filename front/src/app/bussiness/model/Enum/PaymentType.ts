export enum PaymentType{
    phone,
    creditCard,
}

export namespace PaymentType {
    export function values() {
        return Object.keys(PaymentType).filter(
            (type) => isNaN(<any>type) && type !== 'values'
        );
    }
}
