import {Injectable} from '@angular/core';
import {MessageService} from '../../../bussiness/services/message.service';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {Message} from 'app/bussiness/model/message.model';
import {MessageType} from '../../../bussiness/model/Enum/MessageType';

@Injectable()
export class MessageModuleService implements Resolve<any> {

    messages: Message [];

    onMessageChanged: BehaviorSubject<any>;
    onFilterChanged: Subject<any>;
    onFilterByTypeChanged: Subject<any>;
    filteredType: MessageType;

    constructor(private messageService: MessageService) {
        // Set the defaults
        this.onMessageChanged = new BehaviorSubject([]);
        this.onFilterChanged = new Subject();
        this.onFilterByTypeChanged = new Subject();
        this.onFilterByTypeChanged.subscribe(data => {
            this.filteredType = data === '' ? null : +data;
        });
    }


    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {
            Promise.all([this.getMessages()]).then(
                ([files]) => {
                    /*this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getContacts();
                    });*/
                    this.onFilterChanged.subscribe(filter => {

                    });
                    resolve(this.messages);
                }, reject);
        });

    }

    /**
     * Get cities
     *
     * @returns {Promise<any>}
     */
    getMessages(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.messageService.list().subscribe(data => {
                this.messages = data.map(item => {
                    return new Message(item);
                });
                this.onMessageChanged.next(this.messages);

                resolve(data);
            });
        });
    }

    getMessagesBetweenDate(from: any, to: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.messageService.getBetweenDate(from, to).subscribe(data => {
                this.messages = data.map(item => {
                    return new Message(item);
                });
                this.onMessageChanged.next(this.messages);
                resolve(data);
            });
        });
    }

    getMessagesByType(type: any): Promise<any> {
        if (type === '') {
            return this.getMessages();
        }

        return new Promise((resolve, reject) => {
            this.messageService.getByType(type).subscribe(data => {
                this.messages = data.map(item => {
                    return new Message(item);
                });
                this.onMessageChanged.next(this.messages);
                resolve(data);
            });
        });
    }

    updateMessage(id, message): Promise<any> {
        if (id === null) {
            return new Promise((resolve, reject) => {
                this.messageService.create(message).subscribe(response => {
                    this.getMessages();
                    resolve(response);
                });
            });
        } else {
            message._id = id;
            return new Promise((resolve, reject) => {
                this.messageService.update(message).subscribe(response => {
                    this.getMessages();
                    resolve(response);
                });
            });
        }
    }

}
