import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MatProgressBarModule, MatButtonModule, MatIconModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import {UploadsComponent} from 'app/components/upload.component/uploads.component';
import {SeoNestedFormComponent} from 'app/components/seo.nested.form/seo.nested.form';

@NgModule({
    declarations:[
        UploadsComponent,
        SeoNestedFormComponent
    ],
    imports: [
        ReactiveFormsModule,
        CommonModule,
        MatProgressBarModule,
        MatButtonModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule
    ],
    exports: [
        UploadsComponent,
        SeoNestedFormComponent
    ]
})
export class HashveModule
{
}
