import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';

import {StoreService} from 'app/bussiness/services/store.service';
import {CityService} from 'app/bussiness/services/city.service';
import {PackageService} from 'app/bussiness/services/package.service';
import {ItemService} from 'app/bussiness/services/item.service';
import {IStore} from 'app/bussiness/model/interfaces/IStore';
import {from} from 'rxjs';
import * as _ from 'lodash';


@Injectable()
export class StoreComponentService implements Resolve<any> {
    routeParams: any;
    store: any;
    cities: any;
    packages: any;
    items: any;
    onStoreChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onCityChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onPackagesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onItemsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    constructor(
        private http: HttpClient,
        private storeService: StoreService,
        private cityService: CityService,
        private packageService: PackageService,
        private itemService: ItemService,
    ) {
    }

    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;
        return new Promise((resolve, reject) => {
            Promise.all([
                this.getStore(),
                this.getCities(),
                this.getPackages(),
                this.getItems()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    getStore(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.routeParams.id === 'new') {
                this.onStoreChanged.next(false);
                resolve(false);
            } else {
                this.storeService.read(this.routeParams.id)
                    .subscribe((response: any) => {
                        this.store = response;
                        this.onStoreChanged.next(this.store);
                        resolve(response);
                    }, reject);
            }
        });
    }

    getCities(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.routeParams.id === 'new') {
                this.onCityChanged.next(false);
                resolve(false);
            } else {
                this.cityService.list().subscribe((response: any) => {
                    this.cities = response;
                    this.cities.sort((a, b) => {
                        return a.name.heb > b.name.heb ? 1 : -1;
                    });
                    this.onCityChanged.next(this.cities);
                    resolve(response);
                }, reject);
            }
        });
    }

    getPackages(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.routeParams.id === 'new') {
                this.onPackagesChanged.next(false);
                resolve(false);
            } else {
                this.packageService.fullContent().subscribe((response: any) => {
                    this.packages = response;
                    this.onPackagesChanged.next(this.packages);
                    resolve(response);
                }, reject);
            }
        });
    }

    getItems(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.routeParams.id === 'new') {
                this.onItemsChanged.next(false);
                resolve(false);
            } else {
                this.itemService.list().subscribe((response: any) => {
                    this.items = response;
                    this.onItemsChanged.next(this.items);
                    resolve(response);
                }, reject);
            }
        });
    }

    updateStore(store: IStore) {
        return new Promise((resolve, reject) => {
            this.storeService.update(store).subscribe((response: any) => {
                resolve(response);
            }, reject);
        });
    }

    addProduct(product) {
        return new Promise((resolve, reject) => {
            this.http.post('api/e-commerce-products/', product)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
}

