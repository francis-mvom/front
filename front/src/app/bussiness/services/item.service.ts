import {Injectable, EventEmitter} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from './base/service.base';
import {IItem} from '../model/interfaces/IItem';
import {environment} from '../../../environments/environment';

@Injectable()
export class ItemService extends BaseService<IItem> {

    constructor(private http: HttpClient) {
        super(http, environment.apihost, environment.itemEndpoint);
    }
}
