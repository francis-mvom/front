import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
    MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule, MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatTableModule, MatToolbarModule,
    MatSelectModule
} from '@angular/material';
import { ColorPickerModule } from 'ngx-color-picker';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule, FuseSidebarModule } from '@fuse/components';

import { PackageComponent } from 'app/main/apps/package/package.component';
import { PackageModuleService } from 'app/main/apps/package/package.module.service';
import { PackagesPackageListComponent } from 'app/main/apps/package/package-list/package-list.component';
import { PackagesSelectedBarComponent } from 'app/main/apps/package/package-bar/selected-bar.component';
import { PackagesPackageFormDialogComponent } from 'app/main/apps/package/package-form/package-form.component';

const routes: Routes = [
    {
        path     : '**',
        component: PackageComponent,
        resolve  : {
            contacts: PackageModuleService
        }
    }
];

@NgModule({
    declarations   : [
        PackageComponent,
        PackagesPackageListComponent,
        PackagesSelectedBarComponent,
        PackagesPackageFormDialogComponent
    ],
    imports        : [
        RouterModule.forChild(routes),

        ColorPickerModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatTableModule,
        MatToolbarModule,
        MatSelectModule,
        FuseSharedModule,
        FuseConfirmDialogModule,
        FuseSidebarModule
    ],
    providers      : [
        PackageModuleService
    ],
    entryComponents: [
        PackagesPackageFormDialogComponent
    ]
})
export class PackageModule
{
}
