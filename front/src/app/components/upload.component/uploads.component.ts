import { Component, Input, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { FileUploadService } from "../../bussiness/services/file-upload.service";

@Component({
    selector: "app-uploads",
    templateUrl: "./uploads.component.html",
    styleUrls: ["./uploads.component.scss"]
})
export class UploadsComponent implements OnInit {
    show: boolean = false;
    profileForm: FormGroup;
    error: string;

    fileUpload = { status: "", message: "", filePath: "" };

    @Input() path: string;
    @Input() multiple: boolean = false;

    constructor(
        private fb: FormBuilder,
        private fileUploadService: FileUploadService
    ) {}

    ngOnInit() {
        this.profileForm = this.fb.group({
            profile: [""]
        });
    }

    onSelectedFile(event): void {
        if (event.target.files.length > 0) {
            const file = this.multiple
                ? event.target.files
                : event.target.files[0];
            this.profileForm.get("profile").setValue(file);
        }
    }

    onSubmit() {
        console.log(this.profileForm.get("profile").value)
        this.fileUploadService
            .uploadItem(this.profileForm.get("profile").value, this.path)
            .subscribe(
                res => {
                    if (res.status === "progress") {
                        this.fileUpload = res;
                    } else if (res.status === "done") {
                        this.fileUpload = res;
                        this.fileUploadService.onPhotoUploaded.next(
                            this.fileUpload.filePath
                        );
                        this.show = !this.show;
                    }
                },
                err => (this.error = err)
            );
    }
}
