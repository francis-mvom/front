import {Component, ViewEncapsulation} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

import {MatDialog} from '@angular/material';

import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {fuseAnimations} from '@fuse/animations';

import {OrderModuleService} from './order.module.service';
import {Order} from '../../../bussiness/model/order.model';
import {Store} from '../../../bussiness/model/store.model';

/*import {locale as english} from './i18n/en';
import {locale as turkish} from './i18n/tr';*/
/*import {City} from '../../../bussiness/model/city.model';
import {Country} from '../../../bussiness/model/country.model';
import {Region} from '../../../bussiness/model/region.model';*/
/*import {CountriesCountryFormDialogComponent} from './forms/country-form/country-form.component';
import {RegionsRegionFormDialogComponent} from './forms/region-form/region-form.component';
import {CitiesCityFormDialogComponent} from './forms/city-form/city-form.component';*/

@Component({
    selector: 'order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class OrderComponent {
    dialogRef: any;
    hasSelectedContacts: boolean;
    searchInput: FormControl;
    orders: Array<Order>;
    sortFormGroup : FormGroup;
    storesSelected : Array<any> = [] ;
    stores: Store[];

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        private orderModuleService: OrderModuleService,
        private _fuseSidebarService: FuseSidebarService,
        private _matDialog: MatDialog
    ) {
        this.orderModuleService.onOrdersChanged.subscribe(data => {
            this.orders = data;
        });

        const date = new Date();
        this.sortFormGroup = new FormGroup({
            fromDate : new FormControl(new Date(date.getFullYear(), date.getMonth(), 1)),
            toDate :new FormControl(date)
        });
        this.stores = this.orderModuleService.stores;
        this.orderModuleService.onFilterByStoreChanged.next(this.storesSelected);
    }

    /**
     * Toggle the sidebar
     *
     * @param name
     */
    toggleSidebar(name): void
    {
        this._fuseSidebarService.getSidebar(name).toggleOpen();
    }
    sortByDate () :void{
        let toDate = new Date(this.sortFormGroup.get('toDate').value);
        toDate.setHours(23,59,59);

        this.orderModuleService.getOrdersBetweenDate(
            (new Date(this.sortFormGroup.get('fromDate').value).toISOString()),
            (toDate.toISOString()))
    }
    sortByType() :void{
        this.orderModuleService.onFilterByStoreChanged.next(this.storesSelected);
    }
}
