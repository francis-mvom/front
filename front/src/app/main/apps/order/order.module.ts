import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CdkTableModule} from '@angular/cdk/table';
import {HashveModule} from 'app/components/hashve.module';
import { MatTableExporterModule } from 'mat-table-exporter';

import { AngularEditorModule } from '@kolkov/angular-editor';
// import {TdFileService, IUploadOptions} from '@covalent/core/file';
// import {PickListModule} from 'primeng/picklist';

import {
    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatNativeDateModule,
    MatRadioModule,
    MatExpansionModule,
    MatCardModule,

    MatSortModule,
    MatSnackBarModule
} from '@angular/material';

import {FuseSharedModule} from '@fuse/shared.module';
import {FuseConfirmDialogModule} from '@fuse/components';

import {OrderComponent} from './order.component';

import {OrderModuleService} from './order.module.service';
/*import {StoreComponentService} from './store/store.component.service';*/
import {OrdersOrderListComponent} from './order-list/order-list.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import {AgmCoreModule} from '@agm/core';
import {MatDividerModule} from '@angular/material/divider';
import { ProductDetailsComponent } from './product-details/product-details.component';

/*
import {FuseStoreSelectedBarComponent} from './selected-bar/selected-bar.component';
import {FuseStoresStoreFormDialogComponent} from './store-form/store-form.component';*/

// import {FuseStoreInfoComponent} from './store/store.info.component';

const routes: Routes = [
    /*{
      path: ':id',
      component: FuseStoreInfoComponent,
      resolve: {
        data: StoreComponentService
      }
    },
    {
      path: ':id/:handle',
      component: FuseStoreInfoComponent,
      resolve: {
        data: StoreComponentService
      }
    },*/
    {
        path: ':id',
        component: OrderDetailsComponent,
        resolve: {
            order: OrderModuleService
        }
    },
    {
        path: ':id/product/:idProduct',
        component: ProductDetailsComponent,
        resolve: {
            order: OrderModuleService
        }
    },
    {
        path: '',
        component: OrderComponent,
        resolve: {
            stores: OrderModuleService
        }
    },

];

@NgModule({
    declarations: [
        OrderComponent,
        OrdersOrderListComponent,
        OrderDetailsComponent,
        ProductDetailsComponent,
        /*FuseStoreInfoComponent,
        FuseStoreSelectedBarComponent,
        FuseStoresStoreFormDialogComponent,
        FuseReviewFormDialogComponent,*/
    ],
    imports: [
        AngularEditorModule,
        RouterModule.forChild(routes),
        CdkTableModule,
        HashveModule,
        MatNativeDateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSelectModule,
        MatSidenavModule,
        MatTableModule,
        MatTabsModule,
        MatSortModule,
        MatToolbarModule,
        MatRadioModule,
        MatSnackBarModule,
        FuseSharedModule,
        MatDividerModule,
        FuseConfirmDialogModule,
        MatTableExporterModule,
        MatCardModule,
        // CovalentFileModule,
        // CovalentLoadingModule,
        // PickListModule
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        }),
    ],
    providers: [
        // TdFileService,
        OrderModuleService,
        // StoreComponentService
    ],
    entryComponents: [
        /*FuseStoresStoreFormDialogComponent,
        FuseReviewFormDialogComponent*/
    ]
})
export class OrderModule {
}
