import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {
    ActivatedRouteSnapshot,
    Resolve,
    RouterStateSnapshot
} from '@angular/router';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {FuseUtils} from '@fuse/utils';
import {Region} from 'app/bussiness/model/region.model';
import {CityService} from 'app/bussiness/services/city.service';
import {RegionService} from 'app/bussiness/services/region.service';
import {CountryService} from 'app/bussiness/services/country.service';
import {ArticleService} from '../../../bussiness/services/article.service';
import {IArticle} from '../../../bussiness/model/interfaces/IArticle';
import {Article} from '../../../bussiness/model/article.model';

@Injectable()
export class ArticleModuleService implements Resolve<any> {
    onArticlesChanged: BehaviorSubject<any>;

    onFilterChanged: Subject<any>;
    onSearchTextChanged: Subject<any>;

    onSelectedArticlesChanged: BehaviorSubject<any>;

    articles: Article[];

    user: any;
    selectedContacts: string[] = [];
    region: Region;

    searchText: string;
    filterBy: string;

    /**
     * Constructor
     *
     * @param {RegionService} regionService
     * @param {CityService} cityService
     * @param {CountryService} countryService
     */
    constructor(
        private articleService: ArticleService,
    ) {
        // Set the defaults
        this.onArticlesChanged = new BehaviorSubject([]);

        this.onFilterChanged = new Subject();

        /*this.onSelectedContactsChanged = new BehaviorSubject([]);
        this.onUserDataChanged = new BehaviorSubject([]);
        this.onSearchTextChanged = new Subject();
        */
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {
            Promise.all([this.getArticles()]).then(
                ([files]) => {
                    /*this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getContacts();
                    });*/
                    this.onFilterChanged.subscribe(filter => {
                        this.region = filter;
                    });
                    resolve(this.articles);
                }, reject);
        });
    }

    /**
     * Get cities
     *
     * @returns {Promise<any>}
     */
    getArticles(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.articleService.list().subscribe(data => {
                this.articles = data.map(item => {
                    return new Article(item);
                });
                this.onArticlesChanged.next(this.articles);
                resolve(data);
            });
        });
    }

    /**
     * Get user data
     *
     * @returns {Promise<any>}
     */
    /*getUserData(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient
                .get(
                    environment.apihost +
                        "/api/v1/user/5c92b722d1885128c8b0a81f"
                )
                .subscribe((response: any) => {
                    this.user = response;
                    this.onUserDataChanged.next(this.user);
                    resolve(this.user);
                }, reject);
        });
    }*/

    /**
     * Toggle selected contact by id
     *
     * @param id
     */
    /*toggleSelectedContact(id): void {
        // First, check if we already have that contact as selected...
        if (this.selectedContacts.length > 0) {
            const index = this.selectedContacts.indexOf(id);

            if (index !== -1) {
                this.selectedContacts.splice(index, 1);

                // Trigger the next event
                this.onSelectedContactsChanged.next(this.selectedContacts);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedContacts.push(id);

        // Trigger the next event
        this.onSelectedContactsChanged.next(this.selectedContacts);
    }*/

    /**
     * Toggle select all
     */
    /*toggleSelectAll(): void {
        if (this.selectedContacts.length > 0) {
            this.deselectContacts();
        } else {
            this.selectContacts();
        }
    }*/

    /**
     * Select contacts
     *
     * @param filterParameter
     * @param filterValue
     */

    /*selectContacts(filterParameter?, filterValue?): void {
        this.selectedContacts = [];

        // If there is no filter, select all contacts
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedContacts = [];
            this.contacts.map(contact => {
                this.selectedContacts.push(contact._id);
            });
        }

        // Trigger the next event
        this.onSelectedContactsChanged.next(this.selectedContacts);
    }*/

    /**
     * Update country
     *
     * @param contact
     * @returns {Promise<any>}
     */
    updateArticle(id, article): Promise<any> {
        if (id === null) {
            return new Promise((resolve, reject) => {
                this.articleService.create(article).subscribe(response => {
                    this.getArticles();
                    resolve(response);
                });
            });
        } else {
            article._id = id;
            return new Promise((resolve, reject) => {
                this.articleService.update(article).subscribe(response => {
                    this.getArticles();
                    resolve(response);
                });
            });
        }
    }

    /**
     * Delete country
     *
     * @param country
     */
    deleteArticle(article): void {
        this.articleService.delete(article._id).subscribe(data => {
            const Index = this.articles.indexOf(article);
            this.articles.splice(Index, 1);
            this.onArticlesChanged.next(this.articles);
        });
    }
}
