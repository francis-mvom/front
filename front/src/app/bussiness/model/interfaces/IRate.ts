export interface IRate {
    _id?: string;
    date: Date;
    rate: number;
    review: string;
}
