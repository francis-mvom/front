import {ProductOffer} from '../ProductOffer';
import {DiscountType} from '../Enum/DiscountType';
import {DelivertyAddress} from '../DelivertyAddress';
import {CostumerDetails} from '../CostumerDetails';
import {DeliveryDetails} from '../DeliveryDetails';
import {PaymentType} from '../Enum/PaymentType';
import {PhonePayment} from '../PhonePayment';
import {IStatus} from './IStatus';

export interface IOrder {
    _id?: string;
    productOffer: ProductOffer;
    typeOfDiscount: DiscountType;
    deliveryAddress: DelivertyAddress;

    deliveryPrice: number;
    price: number;

    customerDitails?: CostumerDetails;
    deliveryDetails?: DeliveryDetails;
    paymentType: PaymentType;

    creditCardDetails: string;
    phonePaymentDetails: PhonePayment;
    status: Array<IStatus>;
    orderNumber: string;
    createdAt:Date;
}
