import {DestinationType} from './Enum/DestinationType';
import {ReceiverInformation} from './ReceiverInformation';
import {DeliveryType} from './Enum/DeliveryType';

export class DeliveryDetails {
    destinationType: DestinationType = DestinationType.private;
    date: Date;
    hours: string;
    blessing: string;
    specialRequests: string;
    recipient: ReceiverInformation;
    deliveryType:DeliveryType
}
