import {Injectable, EventEmitter} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from './base/service.base';
import {IPackage} from '../model/interfaces/IPackage';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class PackageService extends BaseService<IPackage> {

    constructor(private http: HttpClient) {
        super(http, environment.apihost, environment.packageEndpoint);
    }

    public fullContent(): Observable<IPackage[]>{
        return this.httpClient.get(`${this.url}${this.endpoint}/full-content`).map((data: any) => {
            return data as IPackage[];
        });
    }
}
