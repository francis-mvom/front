import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseSharedModule } from '@fuse/shared.module';
import {AuthGuardService} from '../../bussiness/services/auth-guard.service';


const routes = [
    /*{
        path        : 'dashboards/analytics',
        loadChildren: './dashboards/analytics/analytics.module#AnalyticsDashboardModule'
    },
    {
        path        : 'dashboards/project',
        loadChildren: './dashboards/project/project.module#ProjectDashboardModule'
    },
    {
        path        : 'mail',
        loadChildren: './mail/mail.module#MailModule'
    },
    {
        path        : 'mail-ngrx',
        loadChildren: './mail-ngrx/mail.module#MailNgrxModule'
    },
    {
        path        : 'chat',
        loadChildren: './chat/chat.module#ChatModule'
    },
    {
        path        : 'calendar',
        loadChildren: './calendar/calendar.module#CalendarModule'
    },*/

    {
        path        : 'orders',
        loadChildren: './order/order.module#OrderModule'
    },
    {
        path        : 'stores',
        loadChildren: './store/store.module#StoreModule'
    },
    {
        path        : 'package',
        loadChildren: './package/package.module#PackageModule'
    },
    {
        path        : 'items',
        loadChildren: './item/item.module#ItemModule'
    },
    {
        path        : 'contacts',
        loadChildren: './contacts/contacts.module#ContactsModule'
    },
    {
        path        : 'messages',
       loadChildren: './message/message.module#MessageModule'
      
    },
    {
        path        : 'city',
        loadChildren: './city/city.module#CityModule'
    },
    {
        path        : 'articles',
        loadChildren: './article/article.module#ArticleModule'
    },
    {
        path        : '**',

        loadChildren: './contacts/contacts.module#ContactsModule'
    },


];

@NgModule({
    imports     : [
        RouterModule.forChild(routes),
        FuseSharedModule
    ],
    declarations: []
})
export class AppsModule
{
}
