import {Component, OnDestroy, OnInit} from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';

import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';

import {StoreModuleService} from '../store.module.service';
import {Subject} from 'rxjs';

@Component({
    selector   : 'selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class FuseStoreSelectedBarComponent implements OnInit, OnDestroy
{
    selectedContacts: string[];
    hasSelectedContacts: boolean;
    isIndeterminate: boolean;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private storeModuleService: StoreModuleService,
        public dialog: MatDialog
    )
    {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void
    {
        this.storeModuleService.onSelectedStoresChanged
            .subscribe(selectedStores => {
                this.selectedContacts = selectedStores;
                setTimeout(() => {
                    this.hasSelectedContacts = selectedStores.length > 0;
                    this.isIndeterminate = (selectedStores.length !== this.storeModuleService.stores.length && selectedStores.length > 0);
                }, 0);
            });
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    selectAll(): void
    {
        this.storeModuleService.selectStores();
    }

    deselectAll(): void
    {
        this.storeModuleService.deselectStores();
    }

    deleteSelectedStores(): void
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected contacts?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.storeModuleService.deleteSelectedStores();
            }
            this.confirmDialogRef = null;
        });
    }

}
