import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FuseSidebarService} from '../../../../@fuse/components/sidebar/sidebar.service';
import {MatDialog} from '@angular/material';
import {FormControl, FormGroup} from '@angular/forms';
import {Article} from '../../../bussiness/model/article.model';
import {ArticleModuleService} from './article.module.service';
import {fuseAnimations} from '../../../../@fuse/animations';
import {ArticleFormDialogComponent} from './article-form/article-form.component';
import {Country} from '../../../bussiness/model/country.model';
import {CountriesCountryFormDialogComponent} from '../city/forms/country-form/country-form.component';

@Component({
    selector: 'app-article',
    templateUrl: './article.component.html',
    styleUrls: ['./article.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ArticleComponent implements OnInit {
    dialogRef: any;
    hasSelectedContacts: boolean;
    searchInput: FormControl;
    cities: Array<Article>;

    constructor(private _fuseSidebarService: FuseSidebarService, private _matDialog: MatDialog, private articleService: ArticleModuleService) {
    }

    ngOnInit() {
    }

    newArticle() {
        this.dialogRef = this._matDialog.open(ArticleFormDialogComponent, {
            panelClass: 'contact-form-dialog',
            data: {
                action: 'new',
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                const newArticle: Article = new Article({});
                newArticle._id = response.value._id;
                newArticle.header.en = response.value.header_en;
                newArticle.header.heb = response.value.header_heb;
                newArticle.slogan.en = response.value.slogan_en;
                newArticle.slogan.heb = response.value.slogan_heb;
                newArticle.shitFromLeft.en = response.value.shitFromLeft_en;
                newArticle.shitFromLeft.heb = response.value.shitFromLeft_heb;
                newArticle.text.en = response.value.text_en;
                newArticle.text.heb = response.value.text_heb;
                newArticle.showInList = response.value.showInList;
                newArticle.showOnHomePage = response.value.showOnHomePage;
                newArticle.pic = response.value.pic;
                newArticle.articleType = response.value.articleType;
                newArticle.binding = response.value.binding;
                newArticle.seo = response.value.seo;

                this.articleService.updateArticle(null, newArticle);
            });
    }

    toggleSidebar(name): void {
        this._fuseSidebarService.getSidebar(name).toggleOpen();
    }


}
