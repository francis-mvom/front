import { Component, OnInit, Input, forwardRef } from "@angular/core";
import {
    ControlValueAccessor,
    FormControl,
    FormGroup,
    NG_VALUE_ACCESSOR,
    Validators
} from "@angular/forms";
import { TranslateFormGroupBuilder } from "../../bussiness/model/FormGroupBuilders/TranslateFormGroupBuilder";

@Component({
    selector: "seo-basic-info",
    templateUrl: "./seo.nested.form.html",
    styleUrls: ["./seo.nested.form.scss"],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => SeoNestedFormComponent),
            multi: true
        }
    ]
})
export class SeoNestedFormComponent implements OnInit, ControlValueAccessor {
    private translateBuilder = new TranslateFormGroupBuilder();
    public basicSeoForm: FormGroup = new FormGroup({
        h1: this.translateBuilder.buildGroup({ en: "", heb: "" }),
        title: this.translateBuilder.buildGroup({ en: "", heb: "" }),
        description: this.translateBuilder.buildGroup({ en: "", heb: "" }),
        keywords: this.translateBuilder.buildGroup({ en: "", heb: "" })
    });

    constructor() {}

    ngOnInit() {}

    public onTouched: () => void = () => {};

    writeValue(val: any): void {
        this.basicSeoForm = new FormGroup({
            h1: this.translateBuilder.buildGroup(val && val.h1 ? val.h1 : ""),
            title: this.translateBuilder.buildGroup(val && val.title ? val.title : ""),
            description: this.translateBuilder.buildGroup(
                val && val.description ? val.description : ""
            ),
            keywords: this.translateBuilder.buildGroup(
                val && val.keywords ? val.keywords : ""
            )
        });
    }
    registerOnChange(fn: any): void {
        console.log("on change");
        this.basicSeoForm.valueChanges.subscribe(fn);
    }
    registerOnTouched(fn: any): void {
        console.log("on blur");
        this.onTouched = fn;
    }
    setDisabledState?(isDisabled: boolean): void {
        isDisabled ? this.basicSeoForm.disable() : this.basicSeoForm.enable();
    }
}
