import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
    MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule, MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatTableModule, MatToolbarModule,
    MatSelectModule, MatSortModule, MatListModule, MatGridListModule
} from '@angular/material';
import {TranslateModule} from '@ngx-translate/core';

import {FuseSharedModule} from '@fuse/shared.module';
import {FuseConfirmDialogModule, FuseSidebarModule} from '@fuse/components';
import {HashveModule} from 'app/components/hashve.module';
import {AngularEditorModule} from '@kolkov/angular-editor';
import {ListMessageComponent} from './list-message/list-message.component';
import {MessageComponent} from './message.component';
import {Routes, RouterModule} from '@angular/router';
import {MessageModuleService} from './message.module.service';
import {MessageService} from '../../../bussiness/services/message.service';
import {MessageFormDialogComponent} from './message-form/message-form.component';
import {ScrollingModule} from '@angular/cdk/scrolling';


const routes: Routes = [
    {
        path: '**',
        component: MessageComponent,
        resolve: {
            messages: MessageModuleService
        }
    }
];

@NgModule({
    declarations: [ListMessageComponent, MessageComponent, MessageFormDialogComponent],
    imports: [
        CommonModule,
        AngularEditorModule,
        RouterModule.forChild(routes),
        HashveModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatTableModule,
        MatToolbarModule,
        MatSelectModule,
        MatSortModule,
        MatGridListModule,
        TranslateModule,
        MatListModule,
        FuseConfirmDialogModule,
        FuseSharedModule,
        ScrollingModule
    ],
    providers: [
        MessageService,
        MessageModuleService
    ],
    exports: [
        MessageComponent
    ],
    entryComponents: [
        MessageFormDialogComponent,
    ]
})
export class MessageModule {
}
