import {Injectable, EventEmitter} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from './base/service.base';
import {IRegion} from '../model/interfaces/IRegion';
import {environment} from '../../../environments/environment';

@Injectable()
export class RegionService extends BaseService<IRegion> {

    constructor(private http: HttpClient) {
        super(http, environment.apihost, environment.regionEndpoint);
    }
}
