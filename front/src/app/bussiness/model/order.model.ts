import {ITranslate} from './interfaces/ITranslate';
import {PaymentType} from './Enum/PaymentType';
import {DiscountType} from './Enum/DiscountType';
import {ProductOffer} from './ProductOffer';
import {DelivertyAddress} from './DelivertyAddress';
import {DeliveryDetails} from './DeliveryDetails';
import {PhonePayment} from './PhonePayment';
import {CostumerDetails} from './CostumerDetails';
import {IStatus} from './interfaces/IStatus';

export class Order {
    _id?: string;
    productOffer: ProductOffer;
    typeOfDiscount: DiscountType;
    deliveryAddress: DelivertyAddress;

    deliveryPrice: number;
    price: number;

    customerDitails?: CostumerDetails;
    deliveryDetails?: DeliveryDetails;
    paymentType: PaymentType;

    // creditCardDetails: CreditCardPayment;
    phonePaymentDetails: PhonePayment;
    status: Array<IStatus>;
    creditCardDetails: string;
    orderNumber: string;
    createdAt:Date;
    /**
     * Constructor
     *
     * @param item
     */
    constructor(item: Order = null) {
        {
            this.deliveryAddress = item.deliveryAddress;
            this.phonePaymentDetails = item.phonePaymentDetails;
            this.paymentType = item.paymentType;
            this.deliveryDetails = item.deliveryDetails;
            this.customerDitails = item.customerDitails;
            this.deliveryPrice = item.deliveryPrice;
            this.typeOfDiscount = item.typeOfDiscount;
            this.productOffer = item.productOffer;
            this.price = item.price;
            this._id = item._id;
            this.status = item.status;
            this.creditCardDetails = item.creditCardDetails;
            this.orderNumber = item.orderNumber;
            this.createdAt = item.createdAt;
        }
    }
}
