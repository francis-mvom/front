import {ITranslate} from './ITranslate';
import {IItem} from './IItem';
import {Seo} from '../seo';
import {Category} from '../category.model';

export interface ICategory {
    _id?: string;
    name: ITranslate;
    tagline: ITranslate;
    description: ITranslate;
    pic: string;
    count: number;
    renderOnHomePage: boolean;
    disabled: boolean;
    toTopMenu: boolean;
    toFooterMenu: boolean;
    subcategory: Array<string> | Array<Category>;
    avgPrice: number;
    items: Array<IItem> | Array<string>;
    seo: Seo;
}