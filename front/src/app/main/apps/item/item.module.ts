import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {
    MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule, MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatTableModule, MatToolbarModule,
    MatSelectModule, MatSortModule, MatProgressSpinnerModule
} from '@angular/material';
import {TranslateModule} from '@ngx-translate/core';

import {FuseSharedModule} from '@fuse/shared.module';

import {ItemComponent} from './item.component';
import {ItemsItemListComponent} from './item-list/item-list.component';
import {ItemModuleService} from './item.module.service';
import {ItemsItemFormDialogComponent} from './forms/item-form/item-form.component';
import {CategoriesCategoryFormDialogComponent} from './forms/category-form/category-form.component';
import {FuseConfirmDialogModule, FuseSidebarModule} from '@fuse/components';
import {HashveModule} from 'app/components/hashve.module';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { FileUploadModule } from 'ng2-file-upload';

const routes: Routes = [
    {
        path: '**',
        component: ItemComponent,
        resolve: {
            cities: ItemModuleService
        } 
    }
];

@NgModule({
    declarations: [
        ItemComponent,
        ItemsItemListComponent,
        ItemsItemFormDialogComponent,
        CategoriesCategoryFormDialogComponent,
    ],
    imports: [
        FileUploadModule,
        AngularEditorModule,
        RouterModule.forChild(routes),
        HashveModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatTableModule,
        MatToolbarModule,
        MatSelectModule,
        MatSortModule,
        MatProgressSpinnerModule,
        TranslateModule,

        FuseConfirmDialogModule,
        FuseSharedModule,
    ],
    providers: [
        ItemModuleService
    ],
    exports: [
        ItemComponent
    ],
    entryComponents: [
        ItemsItemFormDialogComponent,
        CategoriesCategoryFormDialogComponent,
    ]
})

export class ItemModule {
}
