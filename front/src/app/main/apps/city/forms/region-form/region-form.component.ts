import {Component, Inject, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {City} from 'app/bussiness/model/city.model';
import {environment} from '../../../../../../environments/environment';
import {Country} from '../../../../../bussiness/model/country.model';
import {Region} from '../../../../../bussiness/model/region.model';
import {FileUploadService} from '../../../../../bussiness/services/file-upload.service';
import {FileUploader} from 'ng2-file-upload';


@Component({
    selector: 'region-contact-form-dialog',
    templateUrl: './region-form.component.html',
    styleUrls: ['./region-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class RegionsRegionFormDialogComponent {
    action: string;
    region: Region;
    countries: Array<Country>;
    regionForm: FormGroup;
    dialogTitle: string;
    apiHost: string = environment.apihost;
    regionPicPath: string = '/assets/region/';
    picPath: string = '';
    uploader: FileUploader = new FileUploader({ url: environment.apihost + environment.uploadFile + environment.uploadRegion, itemAlias: 'file' });
    /**
     * Constructor
     *
     * @param {MatDialogRef<CitiesCityFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<RegionsRegionFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder,
        private fileUploadService: FileUploadService
    ) {
        // Set the defaults
        this.action = _data.action;
        // this.picPath = environment.apihost + environment.uploadFile + environment.uploadRegion;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Region';
            this.region = _data.region;
            this.countries = _data.countries;
        } else {
            this.dialogTitle = 'New Region';
            this.region = new Region({});
            this.countries = _data.countries;
        }

        this.regionForm = this.createRegionForm();

        /*this.fileUploadService.onPhotoUploaded.subscribe(data => {
            this.regionForm.get('pic').setValue(data);
        });*/

        this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            // console.log('FileUpload:uploaded successfully:', item, status, response);
            this.regionForm.get('picture').setValue(JSON.parse(response).data[0].name);
            alert('Your file has been uploaded successfully');
        };
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create contact form
     *
     * @returns {FormGroup}
     */
    createRegionForm(): FormGroup {
        return this._formBuilder.group({
            _id: [this.region._id],
            name_en: [this.region.name.en],
            name_heb: [this.region.name.heb],
            pic: [this.region.pic],
            country: [this.region.country]
        });
    }
}
