export const orderStatuses = [
    {
        'id'   : 1,
        'name' : 'Awaiting check payment',
        'color': 'blue-500'
    },
    {
        'id'   : 2,
        'name' : 'Payment accepted',
        'color': 'green-500'
    },
    {
        'id'   : 3,
        'name' : 'Preparing the order',
        'color': 'red-900'
    }
];