import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {fuseAnimations} from '../../../../../@fuse/animations';
import {FormBuilder, FormGroup} from '@angular/forms';
import {OrderModuleService} from '../order.module.service';
import {orderStatuses} from './order-statuses';
import {Order} from '../../../../bussiness/model/order.model';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';
import {PaymentType} from '../../../../bussiness/model/Enum/PaymentType';
import {environment} from '../../../../../environments/environment';
import {StatusType} from '../../../../bussiness/model/Enum/StatusType';
import {Store} from '../../../../bussiness/model/store.model';
import {Package} from '../../../../bussiness/model/package.model';
import {Item} from '../../../../bussiness/model/item.model';
import { DeliveryType } from 'app/bussiness/model/Enum/DeliveryType';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class OrderDetailsComponent implements OnInit {
    order: Order;
    store: Store;
    pac: Package;
    baseItem: Item;

    orderStatuses: any = StatusType;
    _deliveryType : any = DeliveryType;
    statusForm: FormGroup;
    _payementType : any = PaymentType;

    apiHost: string = environment.apihost;
    statusColors : string[] = ["red-900","blue-500","green-500"];

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {EcommerceOrderService} _ecommerceOrderService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _ordersModuleService: OrderModuleService,
        private route: ActivatedRoute,
        private _formBuilder: FormBuilder
    )
    {
        // Set the defaults
        this.order = this.route.snapshot.data.order ;
        this.store = new Store(this.order.productOffer.store);
        this.pac = new Package(this.order.productOffer.package);
        this.baseItem = new Item(this.pac.baseItem);
        /*this.order.productOffer.store = new Store(this.order.productOffer.store);*/
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    /**
     * On init
     */
    ngOnInit() {
        // Subscribe to update order on changes
      /*  this._ordersModuleService.onOrderSelected
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(order => {
                this.order = new Order(order);

                console.log(order);
            });
*/
        this.statusForm = this._formBuilder.group({
            newStatus: ['']
        });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Update status
     */
    updateStatus(_id): void
    {
        const newStatusId = this.statusForm.get('newStatus').value;

        this._ordersModuleService.updateStatus(_id,{newStatus:newStatusId}).then( order =>{

            this.order = order;

        })

        //this.order.status.unshift(newStatus);
    }

}
