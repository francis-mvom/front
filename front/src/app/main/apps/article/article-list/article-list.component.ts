import {Component, Input, OnInit, TemplateRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {MatDialog, MatDialogRef, MatSort} from '@angular/material';
import {DataSource} from '@angular/cdk/table';
import {merge, Observable, Subject} from 'rxjs';
import {map, takeUntil} from 'rxjs/operators';
import {environment} from '../../../../../environments/environment';
import {fuseAnimations} from '../../../../../@fuse/animations';
import {FuseConfirmDialogComponent} from '../../../../../@fuse/components/confirm-dialog/confirm-dialog.component';

import {ArticleModuleService} from '../article.module.service';
import {ArticleFormDialogComponent} from '../article-form/article-form.component';
import {Article} from '../../../../bussiness/model/article.model';

@Component({
    selector: 'app-article-list',
    templateUrl: './article-list.component.html',
    styleUrls: ['./article-list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ArticleListComponent implements OnInit {
    @ViewChild('dialogContent')
    dialogContent: TemplateRef<any>;
    @ViewChild(MatSort) sort: MatSort;

    articles: Array<Article> = [];
    apiHost: string = environment.apihost;
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'header_heb', 'slogan_heb', 'shitFromLeft_heb', 'buttons'];
    selectedContacts: any[];
    checkboxes: {};
    dialogRef: any;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(private _articlesService: ArticleModuleService,
                public _matDialog: MatDialog) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    ngOnInit() {
        this.dataSource = new FilesDataSource(this._articlesService, this.sort);
        this._articlesService.onArticlesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(articles => {
                this.articles = articles;
                this.checkboxes = {};
                articles.map(article => {
                    this.checkboxes[article._id] = false;
                });
            });
        /*this._citiesService.onSelectedCityChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(selectedContacts => {
                for ( const id in this.checkboxes )
                {
                    if ( !this.checkboxes.hasOwnProperty(id) )
                    {
                        continue;
                    }

                    this.checkboxes[id] = selectedContacts.includes(id);
                }
                this.selectedContacts = selectedContacts;
            });*/

        /*this._contactsService.onUserDataChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(user => {
                this.user = user;
            });*/

        /*this._contactsService.onFilterChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this._contactsService.deselectContacts();
            });*/
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    /**
     * Edit contact
     *
     * @param contact
     */
    editContact(article): void {
        this.dialogRef = this._matDialog.open(ArticleFormDialogComponent, {
            panelClass: 'contact-form-dialog',
            data: {
                article: article,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        const newArticle: Article = new Article({});
                        newArticle._id = formData.value._id;
                        newArticle.header.en = formData.value.header_en;
                        newArticle.header.heb = formData.value.header_heb;
                        newArticle.slogan.en = formData.value.slogan_en;
                        newArticle.slogan.heb = formData.value.slogan_heb;
                        newArticle.shitFromLeft.en = formData.value.shitFromLeft_en;
                        newArticle.shitFromLeft.heb = formData.value.shitFromLeft_heb;
                        newArticle.text.en = formData.value.text_en;
                        newArticle.text.heb = formData.value.text_heb;
                        newArticle.showInList = formData.value.showInList;
                        newArticle.showOnHomePage = formData.value.showOnHomePage;
                        newArticle.pic = formData.value.pic;
                        newArticle.articleType = formData.value.articleType;
                        newArticle.binding = formData.value.binding;
                        newArticle.seo = formData.value.seo;
                        this._articlesService.updateArticle(newArticle._id, newArticle);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteArticle(article);

                        break;
                }
            });
    }

    /*/!**
     * Delete City
     *!/*/
    deleteArticle(article): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this._articlesService.deleteArticle(article);
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(id){
        //this._contactsService.toggleSelectedContact(contactId);
    }
}

export class FilesDataSource extends DataSource<any> {
    /**
     * Constructor
     *
     * @param {ArticleModuleService} _citiesService
     */
    constructor(
        private _articleService: ArticleModuleService,
        private _matSort: MatSort
    ) {
        super();
    }

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            this._articleService.onArticlesChanged,
            // this._matPaginator.page,
            this._articleService.onFilterChanged,
            this._matSort.sortChange
        ];

        return merge(...displayDataChanges).pipe(
            map(() => {
                let data = this._articleService.articles.slice();
                /*data = data.filter(item => this._articleService.region ? this._articleService.region._id === item.region : true);*/
                data = this.sortData(data);
                return data;
            })
        );
    }

    /**
     * Sort data
     *
     * @param data
     * @returns {any[]}
     */
    sortData(data): any[] {
        if (!this._matSort.active || this._matSort.direction === '') {
            return data;
        }

        return data.sort((a, b) => {
            let propertyA: number | string = '';
            let propertyB: number | string = '';
            switch (this._matSort.active) {
                case 'header_en':
                    [propertyA, propertyB] = [a.header.en, b.header.en];
                    break;
                case 'header_heb':
                    [propertyA, propertyB] = [a.header.heb, b.header.heb];
                    break;
                case 'slogan_en':
                    [propertyA, propertyB] = [a.slogan.en, b.slogan.en];
                    break;
                case 'slogan_heb':
                    [propertyA, propertyB] = [a.slogan.heb, b.slogan.heb];
                    break;
                case 'shitFromLeft_en':
                    [propertyA, propertyB] = [a.shitFromLeft.en, b.shitFromLeft.en];
                    break;
                case 'shitFromLeft_heb':
                    [propertyA, propertyB] = [a.shitFromLeft.heb, b.shitFromLeft.heb];
                    break;
            }

            const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

            return (valueA < valueB ? -1 : 1) * (this._matSort.direction === 'asc' ? 1 : -1);
        });
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}
