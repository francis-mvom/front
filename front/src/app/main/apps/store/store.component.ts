import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {MatDialog} from '@angular/material';

import {Subscription} from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';

import {FuseStoresStoreFormDialogComponent} from './store-form/store-form.component';
import {IStore} from 'app/bussiness/model/interfaces/IStore';
import {Store} from 'app/bussiness/model/store.model';

import {StoreModuleService} from './store.module.service';

@Component({
    selector: 'fuse-store',
    templateUrl: './store.component.html',
    styleUrls: ['./store.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class FuseStoreComponent implements OnInit, OnDestroy {
    hasSelectedStores: boolean;
    searchInput: FormControl;
    dialogRef: any;

    carTypeDialogRef: any;
    cityDialogRef: any;
    sortFormGroup : FormGroup;
    stores : Array<Store>;
    storesSelected : Array<any> = [] ;

    onSelectedStoresChangedSubscription: Subscription;

    constructor(
        private storeModuleService: StoreModuleService,
        private route: ActivatedRoute,
        public dialog: MatDialog
    ) {
        this.searchInput = new FormControl('');
        this.stores = this.storeModuleService.stores;



    }

    newStore(): void {
        this.dialogRef = this.dialog.open(FuseStoresStoreFormDialogComponent, {
            panelClass: 'contact-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                const newStore: IStore = new Store({});
                newStore.name.en = response.value.name_en;
                newStore.name.heb = response.value.name_heb;
                newStore.description.en = response.value.description_en;
                newStore.description.heb = response.value.description_heb;
                newStore.address.en = response.value.address_en;
                newStore.address.heb = response.value.address_heb;

                newStore.tel = response.value.tel;
                newStore.fax = response.value.fax;
                newStore.mobile = response.value.mobile;
                newStore.mail = response.value.mail;

                this.storeModuleService.updateStore(null, newStore).then((store: IStore) => {
                    this.storeModuleService.onSearchTextChanged.next('');
                });

            });

    }

    ngOnInit() {
        this.onSelectedStoresChangedSubscription =
            this.storeModuleService.onSelectedStoresChanged
                .subscribe(selectedStores => {
                    this.hasSelectedStores = selectedStores.length > 0;
                });

        this.searchInput.valueChanges
            .pipe(
                debounceTime(300),
                distinctUntilChanged()
            )
            .subscribe(searchText => {
                this.storeModuleService.onSearchTextChanged.next(searchText);
            });
        this.storeModuleService.onFilterChanged.next(this.storesSelected);
    }

    ngOnDestroy() {
        this.onSelectedStoresChangedSubscription.unsubscribe();
    }



}
