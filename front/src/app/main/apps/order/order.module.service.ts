import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import {
    ActivatedRouteSnapshot,
    Resolve,
    RouterStateSnapshot
} from "@angular/router";
import { BehaviorSubject, Observable, Subject } from "rxjs";

import { FuseUtils } from "@fuse/utils";

import { Store } from "app/bussiness/model/store.model";
import { OrderService } from "app/bussiness/services/order.service";

import {Order} from '../../../bussiness/model/order.model';
import {ItemService} from '../../../bussiness/services/item.service';
import {StoreService} from '../../../bussiness/services/store.service';

@Injectable()
export class OrderModuleService implements Resolve<any> {
    onOrdersChanged: BehaviorSubject<any>;
    onSelectedOrdersChanged: BehaviorSubject<any>;
    onOrderSelected: BehaviorSubject<any>;
    onSearchTextChanged: Subject<any>;
    onFilterChanged: Subject<any>;
    onFilterByStoreChanged: Subject<any>;
    updateOrderTmp :Order
    orders: Order[];
    selectedOrders: string[] = [];

    searchText: string;
    filterBy: string;
    filterByStore: Array<any> = [];
    stores: Store[];
    toDate :Date ;
    fromDate : Date

    /**
     * Constructor
     *
     * @param {StoreService} storeService
     */
    constructor(private orderService: OrderService, private itemService : ItemService, private storeService: StoreService, private http: HttpClient) {
        // Set the defaults
        this.onOrdersChanged = new BehaviorSubject([]);
        this.onSelectedOrdersChanged = new BehaviorSubject([]);
        this.onOrderSelected =  new BehaviorSubject({});
        this.onSearchTextChanged = new Subject();
        this.onFilterChanged = new Subject();
        this.onFilterByStoreChanged = new Subject();

        // init date for sort data
        this.toDate =  new Date();
        this.fromDate = new Date(this.toDate.getFullYear(), this.toDate.getMonth(), 1)

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> | Promise<any> | any {
        const orderId = route.params.id;
        const productId =  route.params.idProduct;

        if(productId){
            return new Promise((resolve, reject) => {
                this.itemService.read(productId).subscribe(data => {
                    resolve({product: data, orderId: orderId});
                })
             ,reject});

        }else if(orderId){
           return this.orderService.read(orderId);
        }else{
            return new Promise((resolve, reject) => {
                Promise.all([
                    this.getOrders(),
                    this.getStores()
                ]).then(([files]) => {
                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getOrders();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getOrders();
                    });
                    this.onFilterByStoreChanged.subscribe(filter => {
                        this.filterByStore = filter;

                    });

                    resolve();
                }, reject);
            });
        }
    }

    /**
     * Get contacts
     *
     * @returns {Promise<any>}
     */
    getOrders(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.orderService.getBetweenDate(this.fromDate.toISOString(), this.toDate.toISOString()).subscribe(data => {
                this.orders = data;
                if (this.searchText && this.searchText !== "") {
                    this.orders = FuseUtils.filterArrayByString(
                        this.orders,
                        this.searchText
                    );
                }

                this.orders = this.orders.map(order => {
                    return new Order(order);
                });

                this.onOrdersChanged.next(this.orders);
                resolve(this.orders);
            }, reject);
        });
    }

    /**
     * Toggle selected contact by id
     *
     * @param id
     */
    toggleSelectedOrders(id): void {
        // First, check if we already have that contact as selected...
        if (this.selectedOrders.length > 0) {
            const index = this.selectedOrders.indexOf(id);

            if (index !== -1) {
                this.selectedOrders.splice(index, 1);

                // Trigger the next event
                this.onSelectedOrdersChanged.next(this.selectedOrders);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedOrders.push(id);

        // Trigger the next event
        this.onSelectedOrdersChanged.next(this.selectedOrders);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll(): void {
        if (this.selectedOrders.length > 0) {
            this.deselectOrders();
        } else {
            this.selectOrders();
        }
    }

    /**
     * selected one order
     * * @param order
     */
    toggleSelectOneOrder(order): void {
        this.onOrderSelected.next(order);
    }

    /**
     * Select contacts
     *
     * @param filterParameter
     * @param filterValue
     */
    selectOrders(filterParameter?, filterValue?): void {
        this.selectedOrders = [];

        // If there is no filter, select all contacts
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedOrders = [];
            this.orders.map(order => {
                this.selectedOrders.push(order._id);
            });
        }

        // Trigger the next event
        this.onSelectedOrdersChanged.next(this.selectedOrders);
    }

    /**
     * Update contact
     *
     * @param contact
     * @returns {Promise<any>}
     */
    updateStore(id, order): Promise<any> {
        if(id === null){
            return new Promise((resolve, reject) => {
                this.orderService.create(order)
                    .subscribe(response => {
                        this.getOrders();
                        resolve(response);
                    });
            });
        }else{
            return new Promise((resolve, reject) => {
                this.orderService.update(order)
                    .subscribe(response => {
                        this.getOrders();
                        resolve(response);
                    });
            });
        }
    }

    /**
     * Deselect contacts
     */
    deselectOrders(): void {
        this.selectedOrders = [];

        // Trigger the next event
        this.onSelectedOrdersChanged.next(this.selectedOrders);
    }

    /**
     * Delete contact
     *
     * @param contact
     */
    deleteOrder(order): void {
        const orderIndex = this.orders.indexOf(order);
        this.orders.splice(orderIndex, 1);
        this.onOrdersChanged.next(this.orders);
    }

    /**
     * Delete selected contacts
     */
    deleteSelectedOrders(): void {
        for (const orderId of this.selectedOrders) {
            const order = this.orders.find(_order => {
                return _order._id === orderId;
            });
            const orderIndex = this.orders.indexOf(order);
            this.orders.splice(orderIndex, 1);
        }
        this.onOrdersChanged.next(this.orders);
        this.deselectOrders();
    }

    /**
     * Update contact
     *
     * @param contact
     * @returns {Promise<any>}
     */
     updateStatus(id,status):Promise<any> {
     return new Promise((resolve, reject) => {
                this.orderService.updateStatus(id,status)
                    .subscribe(data => {
                        let order = new Order(data);

                        this.getOrders();
                        resolve(order);
                    });
            });
        }

    /**
     * sort store by date
     */
    getOrdersBetweenDate(from: any, to: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.orderService.getBetweenDate(from, to).subscribe(data => {
                this.orders = data.map(item => {
                    return new Order(item);
                });
                this.onOrdersChanged.next(this.orders);
                resolve(data);
            });
        });
    }
    /**
     * Get list of store
     */
    getStores(): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.storeService.getNameAndId().subscribe(response =>{
                this.stores = response ;

                resolve(this.stores);
            });

        });
    }

}
