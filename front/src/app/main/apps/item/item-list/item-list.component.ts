import {Component, Input, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {MatDialog, MatDialogRef, MatSort} from '@angular/material';
import {DataSource} from '@angular/cdk/collections';
import {merge, Observable, Subject} from 'rxjs';
import {map, takeUntil} from 'rxjs/operators';
import {environment} from '../../../../../environments/environment';
import {fuseAnimations} from '@fuse/animations';
import {FuseConfirmDialogComponent} from '@fuse/components/confirm-dialog/confirm-dialog.component';
import {Category} from 'app/bussiness/model/category.model';
import {Item} from 'app/bussiness/model/item.model';
import {ItemModuleService} from 'app/main/apps/item/item.module.service';
import * as _ from 'lodash';
import {ItemsItemFormDialogComponent} from '../forms/item-form/item-form.component';

@Component({
    selector: 'item-contact-list',
    templateUrl: './item-list.component.html',
    styleUrls: ['./item-list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ItemsItemListComponent implements OnInit, OnDestroy {
    @ViewChild('dialogContent')
    dialogContent: TemplateRef<any>;
    @ViewChild(MatSort) sort: MatSort;

    @Input()
    set category(val: Category) {
        this.itemModuleService.onFilterChanged.next(val);
    }

    items: Array<Item>;
    categories: Array<Category>;
    apiHost: string = environment.apihost;
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'avatar', 'name_heb', 'name_en', 'code', 'recommended', 'additional', 'dalydeal', 'inStock', 'price', 'buttons'];
    selectedContacts: any[];
    checkboxes: {};
    dialogRef: any;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    itemPicPath: string = '/assets/items/';

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ContactsService} _contactsService
     * @param {MatDialog} _matDialog
     */
    constructor(
        private itemModuleService: ItemModuleService,
        public _matDialog: MatDialog
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.dataSource = new FilesDataSource(this.itemModuleService, this.sort);

        this.itemModuleService.onItemsChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(items => {
                this.items = items;

                this.checkboxes = {};
                items.map(item => {
                    this.checkboxes[item._id] = false;
                });
            });

        this.itemModuleService.onCategoriesChanged
            .subscribe(categories => {
                this.categories = categories;
            });

        /*this._contactsService.onFilterChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this._contactsService.deselectContacts();
            });*/
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    getCategory(category): string {
        const index = _.findIndex(this.categories, item => item._id === category);
        if (index > -1) {
            return this.categories[index].name.heb as string;
        } else {
            return 'No Category';
        }
    }

    /**
     * Edit item
     *
     * @param contact
     */

    editItem(clientValue: Item): void {
        this.dialogRef = this._matDialog.open(ItemsItemFormDialogComponent, {
            panelClass: 'contact-form-dialog',
            data: {
                action: 'edit',
                item: clientValue,
                categories: this.categories
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':

                        const newItem: Item = new Item({});
                        newItem._id = response[1].value._id;
                        newItem.name.en = response[1].value.name_en;
                        newItem.name.heb = response[1].value.name_heb;
                        newItem.description.en = response[1].value.description_en;
                        newItem.description.heb = response[1].value.description_heb;
                        newItem.code = response[1].value.code;
                        newItem.rate = response[1].value.rate;
                        newItem.picture = response[1].value.picture;
                        newItem.additional = response[1].value.additional;
                        newItem.recommended = response[1].value.recommended;
                        newItem.inStock = response[1].value.inStock;
                        newItem.category = response[1].value.category;
                        newItem.dayDeal = response[1].value.deal;
                        newItem.price = response[1].value.price;
                        newItem.seo = response[1].value.seo;

                        this.itemModuleService.updateItem(newItem._id, newItem);
                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteItem(clientValue);

                        break;
                }
            });
    }

    /*
     * Delete Item
     */
    deleteItem(item): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                console.log(item);
                this.itemModuleService.deleteItem(item);
            }
            this.confirmDialogRef = null;
        });

    }

    /*
        /**
         * On selected change
         *
         * @param contactId
         */
    /*onSelectedChange(contactId): void
    {
        this._contactsService.toggleSelectedContact(contactId);
    }*/

    /**
     * Toggle star
     *
     * @param contactId
     */
    /*toggleStar(contactId): void
    {
        if ( this.user.starred.includes(contactId) )
        {
            this.user.starred.splice(this.user.starred.indexOf(contactId), 1);
        }
        else
        {
            this.user.starred.push(contactId);
        }

        this._contactsService.updateUserData(this.user);
    }*/
}

export class FilesDataSource extends DataSource<any> {
    /**
     * Constructor
     *
     * @param {ItemModuleService} itemService
     */
    constructor(
        private itemService: ItemModuleService,
        private _matSort: MatSort,
    ) {
        super();
    }

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            this.itemService.onItemsChanged,
            this.itemService.onFilterChanged,
            this._matSort.sortChange
        ];
        return merge(...displayDataChanges).pipe(
            map(() => {
                let data = this.itemService.items.slice();
                data = data.filter(item => this.itemService.category ? (item.category as Array<string>).indexOf(this.itemService.category._id) > -1 : true);
                data = this.sortData(data);
                return data;
            })
        );
    }

    sortData(data): any[] {
        if (!this._matSort.active || this._matSort.direction === '') {
            return data;
        }

        return data.sort((a, b) => {
            let propertyA: number | string = '';
            let propertyB: number | string = '';
            switch (this._matSort.active) {
                case 'name_en':
                    [propertyA, propertyB] = [a.name.en, b.name.en];
                    break;
                case 'name_heb':
                    [propertyA, propertyB] = [a.name.heb, b.name.heb];
                    break;
                case 'code':
                    [propertyA, propertyB] = [a.code, b.code];
                    break;
            }

            const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

            return (valueA < valueB ? -1 : 1) * (this._matSort.direction === 'asc' ? 1 : -1);
        });
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}
