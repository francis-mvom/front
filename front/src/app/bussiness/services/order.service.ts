import {Injectable, EventEmitter} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from './base/service.base';
import {environment} from '../../../environments/environment';
import { IOrder } from '../model/interfaces/IOrder';
import {Observable} from 'rxjs';
import {IStore} from '../model/interfaces/IStore';

@Injectable()
export class OrderService extends BaseService<IOrder> {
    constructor(private http: HttpClient) {
        super(http, environment.apihost, environment.orderEndpoint);
    }
    public updateStatus (_id:string,new_status:any) : Observable<IOrder> {
        return this.httpClient.put(`${this.url}${this.endpoint}/update-status/${_id}`,new_status).map((data: any) => {
            return data as IOrder;
        });
    }
    public getBetweenDate (from:any,to:any) : Observable<IOrder[]> {

        return this.httpClient.get(`${this.url}${this.endpoint}/get-by-date/${from}/${to}`).map((data: any) => {
            return data as IOrder[];
        });
    }
}
