import {Injectable, EventEmitter} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from './base/service.base';
import {ICategory} from '../model/interfaces/ICategory';
import {environment} from '../../../environments/environment';

@Injectable()
export class CategoryService extends BaseService<ICategory> {

    constructor(private http: HttpClient) {
        super(http, environment.apihost, environment.categoryEndpoint);
    }
}
