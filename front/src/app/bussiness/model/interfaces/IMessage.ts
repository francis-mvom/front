import { ITranslate } from './ITranslate';
import { MessageType } from '../Enum/MessageType';
import { IUser } from './IUser';

export interface IMessage {
    _id?: string ;
    title: string;
    text: string;
    type: MessageType | number ;
    theme : string;
    name : string ;
    email : string ;
    phone : string ;
    new : boolean
    from: IUser | string; 
    to: IUser | string;
}
