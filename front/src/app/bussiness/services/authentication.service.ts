import {Injectable} from "@angular/core/";
import {Observable} from "rxjs";
import {UserService} from "../services/user.service";

@Injectable()
export class AuthenticationService {
    constructor(protected userService: UserService) { }

    getToken(): string {
        return localStorage.getItem('token');
    }

    isLoggedIn() {
        const token = this.getToken();
        return token != null;
    }

    login(email: string, password: string): Observable<any> {
        return this.userService.login(email, password)
        //this is a mocked response to be able to test the example
        /*return new Observable((observer) => {
            if (email === this.testUser.email && password === this.testUser.password) {
                observer.next({email: this.testUser.email, token: this.testUser.token});
            } else {
                observer.error({error: 'invalid credentials.'});
            }
            observer.complete();
        });*/
        //this would probably make an http post to the server to process the login
        //return this.http.post<User>(url, {email, password});
    }
}