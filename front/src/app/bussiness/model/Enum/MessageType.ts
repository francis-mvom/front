export enum MessageType {
    CONTACT_FORM,
    CUSTOMER_MESSAGES,
}

export namespace MessageType {
    export function values() {
        return Object.keys(MessageType).filter(
            (type) => isNaN(<any>type) && type !== 'values'
        );
    }
}