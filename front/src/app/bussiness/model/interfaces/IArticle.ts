import {ITranslate} from './ITranslate';
import {Seo} from '../seo';
import {ArticleType} from '../Enum/ArticleType';
import {BindingType} from '../Enum/BindingType';
import {ICity} from './ICity';
import {IRegion} from './IRegion';

export interface IArticle {
    _id?: string;
    header: ITranslate;
    slogan: ITranslate;
    shitFromLeft: ITranslate;
    text: ITranslate;
    pic: String;
    showInList: boolean;
    showOnHomePage: boolean;
    articleType: ArticleType;
    binding: {
        bindingType: BindingType,
        object: String | ICity | IRegion;
    };
    seo: Seo;
}