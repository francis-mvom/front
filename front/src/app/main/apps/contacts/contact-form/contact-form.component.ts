import {Component, Inject, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

import {Contact} from 'app/bussiness/model/contact.model';
import {environment} from '../../../../../environments/environment';
import {Store} from '../../../../bussiness/model/store.model';

@Component({
    selector: 'contacts-contact-form-dialog',
    templateUrl: './contact-form.component.html',
    styleUrls: ['./contact-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ContactsContactFormDialogComponent {
    action: string;
    contact: Contact; 
    contactForm: FormGroup;
    dialogTitle: string;
    roleUser : any;
    apiHost: string = environment.apihost;
    _stores: Store[];

    /**
     * Constructor
     *
     * @param {MatDialogRef<ContactsContactFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<ContactsContactFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder
    ) {
        // Set the defaults
        this.action = _data.action;
        this._stores = this._data.stores ;


        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Contact';
            this.contact = _data.contact;
        } else {
            this.dialogTitle = 'New Contact';
            this.contact = new Contact({});
        }

        this.contactForm = this.createContactForm();

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create contact form
     *
     * @returns {FormGroup}
     */
    createContactForm(): FormGroup {


        return this._formBuilder.group({
            _id: [this.contact._id],
            fullName: [this.contact.fullName],
            country: [this.contact.country],
            pic: [this.contact.pic],
            userType: [this.contact.userType],
            mail: [this.contact.mail],
            mobile: [this.contact.mobile],
            password: '',
            confirmPassword: '',
            stores: [this.contact.stores],
            birthday: [this.contact.birthday],
        });
    }

}
