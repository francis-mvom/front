import {ITranslate} from './ITranslate';
import {Location} from '../location.model';
import {WorkingHours} from '../working.hours.model';
import {IRate} from './IRate';
import {PaymentType} from '../Enum/PaymentType';
import {DeliveryType} from '../Enum/DeliveryType';
import {Delivery} from '../Delivery';
import {PackageStoreOffer} from '../package.store.offer';
import {ItemStoreOffer} from '../item.store.offer';
import {Seo} from '../seo';
import {ICity} from './ICity';

export interface IStore {
    _id?: string;
    name: ITranslate;
    pic: Array<string>;
    city: ICity | string;
    link: string;
    ind: number;
    toHomePage: boolean;
    address: ITranslate;
    tel: string;
    fax: string;
    mobile: string;
    mail: string;
    description: ITranslate;
    // что это такое
    adminActive: Boolean;
    active: Boolean;
    callToStore: Boolean;
    callNumber: Boolean;
    message: string;
    adminMessage: string;
    payments: Array<PaymentType>;
    deliveryTypes: Array<DeliveryType>;
    items: Array<ItemStoreOffer>;
    delivery: Array<Delivery>;
    // deliveryPlaces: Array<Delivery>;
    packages: Array<PackageStoreOffer>;
    storeLocation: Location;
    rate: Array<IRate>;
    weekHours: WorkingHours;
    weekEndHours: WorkingHours;
    shabatHours: WorkingHours;
    seo: Seo;
}
