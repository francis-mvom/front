import {Component, Inject, ViewEncapsulation} from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {City} from 'app/bussiness/model/city.model';
import {environment} from '../../../../../../environments/environment';
import {CityModuleService} from '../../city.module.service';
import {Country} from '../../../../../bussiness/model/country.model';
import {Region} from '../../../../../bussiness/model/region.model';
import * as _ from 'lodash';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import {FileUploadService} from '../../../../../bussiness/services/file-upload.service';
import {FileUploader} from 'ng2-file-upload';


@Component({
    selector: 'city-contact-form-dialog',
    templateUrl: './city-form.component.html',
    styleUrls: ['./city-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class CitiesCityFormDialogComponent {
    editorConfig: AngularEditorConfig = {
        editable: true,
        spellcheck: true,
        height: '25rem',
        minHeight: '5rem',
        placeholder: 'Enter text here...',
        translate: 'no',
        uploadUrl: 'v1/images', // if needed
        customClasses: [ // optional
            {
                name: "quote",
                class: "quote",
            },
            {
                name: 'redText',
                class: 'redText'
            },
            {
                name: "titleText",
                class: "titleText",
                tag: "h1",
            },
        ]
    };
    action: string;
    city: City;
    countries: Array<Country>;
    regions: Array<Region>;
    selectedRegions: Array<Region> = [];
    cityForm: FormGroup;
    dialogTitle: string;
    cityPicPath: string = '/assets/city/';
    apiHost: string = environment.apihost;
    picPath:string = '';
    public uploader: FileUploader = new FileUploader({ url: environment.apihost + environment.uploadFile + environment.uploadCity, itemAlias: 'file' });
    /**
     * Constructor
     *
     * @param {MatDialogRef<CitiesCityFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<CitiesCityFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder,
        /*private fileUploadService: FileUploadService,
        private cityModuleService: CityModuleService*/
    ) {
        // Set the defaults
        this.action = _data.action;
        this.countries = _data.countries;
        this.regions = _data.regions;
        if (this.action === 'edit') {
            this.dialogTitle = 'Edit City';
            this.city = _data.city;
            this.selectedRegions = _.filter(this.regions, (o) => {
                return o.country === this.city.country;
            });
        } else {
            this.dialogTitle = 'New City';
            this.city = new City({});
        }

        this.cityForm = this.createContactForm();

        /*this.picPath = environment.apihost + environment.uploadFile + environment.uploadCity;
        this.fileUploadService.onPhotoUploaded.subscribe(data => {
            this.cityForm.get('pic').setValue(data);
        });*/

        this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            // console.log('FileUpload:uploaded successfully:', item, status, response);
            this.cityForm.get('pic').setValue(JSON.parse(response).data[0].name);
            alert('Your file has been uploaded successfully');
        };
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create contact form
     *
     * @returns {FormGroup}
     */
    createContactForm(): FormGroup {
        return this._formBuilder.group({
            _id: [this.city._id],
            name_en: [this.city.name.en],
            name_heb: [this.city.name.heb],
            description_heb: [this.city.description.heb],
            description_en: [this.city.description.en],
            pic: [this.city.pic],
            inPopular: [this.city.inPopular],
            inReview: [this.city.inReview],
            region: [this.city.region],
            country: [this.city.country],
            seo: new FormControl(this.city.seo)
        });
    }

    onCountrySelect(stateName): void{
        this.selectedRegions = _.filter(this.regions, function(o) {
            return o.country === stateName;
        });
    }
}
