import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import {map, switchMap, tap} from 'rxjs/operators';
import {AuthenticationService} from '../../services/authentication.service';
import {AuthenticationActionTypes, Login, LoginFailure, LoginSuccess} from '../actions/authentication.actions';
import {Observable, of} from 'rxjs';
import 'rxjs/add/observable/of';



@Injectable()
export class AuthEffects {

    constructor(
        private actions: Actions,
        private authService: AuthenticationService,
        private router: Router,
    ) {

    }

    // effects go here
    @Effect()
    LogIn: Observable<any> = this.actions.pipe(
        ofType(AuthenticationActionTypes.LOGIN),
        map((action: Login) => action.payload),
        switchMap(payload => {
            return this.authService.login(payload.email, payload.password)
                .map((response) => {

                    if(typeof  response.success !== 'undefined')
                    {
                        return new LoginFailure({ error: response });
                    }
                    return new LoginSuccess({response: response , email: payload.email});
                })
                .catch((error) => {
                    console.error(error);
                    return of(new LoginFailure({ error: error }));
                });
        }));

    //effect login success
    @Effect({ dispatch: false })
    LogInSuccess: Observable<any> = this.actions.pipe(
        ofType(AuthenticationActionTypes.LOGIN_SUCCESS),
        tap((data) => {
            localStorage.setItem('token', JSON.stringify(data.payload.response.token));
            localStorage.setItem('user', JSON.stringify(data.payload.response.user));
            this.router.navigateByUrl('/apps');
        })
    );
    //effect login failure
    @Effect({ dispatch: false })
    LogInFailure: Observable<any> = this.actions.pipe(
        ofType(AuthenticationActionTypes.LOGIN_FAILURE),
        tap((user) => {

        })
    );

    //effects logout
    @Effect({ dispatch: false })
    public LogOut: Observable<any> = this.actions.pipe(
        ofType(AuthenticationActionTypes.LOGOUT),
        tap((user) => {
            localStorage.removeItem('token');
            localStorage.removeItem('user');
            this.router.navigateByUrl('/');
        })
    );
}