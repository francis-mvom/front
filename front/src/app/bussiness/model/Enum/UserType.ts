export enum UserType{
    admin = 'Admin',
    storeAdmin = 'StoreAdmin',
    user = 'User',
}
export namespace UserType {
    export function values() {
        return Object.keys(UserType).filter(
            (type) => isNaN(<any>type) && type !== 'values'
        );
    }
}