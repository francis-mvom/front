import {ITranslate} from './interfaces/ITranslate';
import {ICategory} from './interfaces/ICategory';
import {IItem} from './interfaces/IItem';
import {IRate} from './interfaces/IRate';
import {Seo} from './seo';

export class Item implements IItem {
    _id: string;
    name: ITranslate;
    description: ITranslate;
    code: string;
    picture: string;
    additional: boolean;
    recommended: boolean;
    inStock: boolean;
    rate: Array<IRate>;
    category: Array<string> | Array<ICategory>;
    dayDeal: boolean;
    price: number;
    seo: Seo;

    /**
     * Constructor
     *
     * @param category
     */
    constructor(item) {
        {
            this._id = item._id || '';
            this.name = item.name || {heb: '', en: ''};
            this.description = item.description || {heb: '', en: ''};
            this.code = item.code || '';
            this.picture = (item.picture === '' || item.picture === undefined) ? '' : item.picture;
            this.additional = item.additional === undefined || item.additional === null ? true : item.additional;
            this.recommended = item.recommended === undefined || item.recommended === null ? false : item.recommended;
            this.inStock = item.inStock === undefined || item.inStock === null ? false : item.inStock;
            this.rate = item.rate || [];
            this.category = item.category || '';
            this.dayDeal = item.dayDeal === undefined || item.dayDeal === null ? false : item.dayDeal;
            this.price = item.price || 0;
            this.seo = item.seo;
        }
    }
}
