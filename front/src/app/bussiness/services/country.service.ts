import {Injectable, EventEmitter} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from './base/service.base';
import {ICountry} from '../model/interfaces/ICountry';
import {environment} from '../../../environments/environment';

@Injectable()
export class CountryService extends BaseService<ICountry> {

    constructor(private http: HttpClient) {
        super(http, environment.apihost, environment.countryEndpoint);
    }
}
