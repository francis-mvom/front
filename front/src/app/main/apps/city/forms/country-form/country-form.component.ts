import {Component, Inject, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Country} from 'app/bussiness/model/country.model';
import {environment} from '../../../../../../environments/environment';


@Component({
    selector: 'country-contact-form-dialog',
    templateUrl: './country-form.component.html',
    styleUrls: ['./country-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class CountriesCountryFormDialogComponent {
    action: string;
    country: Country;
    countryForm: FormGroup;
    dialogTitle: string;
    apiHost: string = environment.apihost;

    /**
     * Constructor
     *
     * @param {MatDialogRef<CountriesCountryFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<CountriesCountryFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder
    ) {
        // Set the defaults
        this.action = _data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Country';
            this.country = _data.country;
        } else {
            this.dialogTitle = 'New Country';
            this.country = new Country({});
        }

        this.countryForm = this.createContactForm();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create contact form
     *
     * @returns {FormGroup}
     */
    createContactForm(): FormGroup {
        return this._formBuilder.group({
            _id: [this.country._id],
            name_en: [this.country.name.en],
            name_heb: [this.country.name.heb],
            regions: [this.country.regions],
        });
    }
}
