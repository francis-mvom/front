import { FuseUtils } from '@fuse/utils';
import { IStore } from './interfaces/IStore';
import { ITranslate } from './interfaces/ITranslate';
import { PaymentType } from './Enum/PaymentType';
import {DeliveryType} from './Enum/DeliveryType';
import { ItemStoreOffer } from './item.store.offer';
import {Delivery} from './Delivery';
import {PackageStoreOffer} from './package.store.offer';
import {Location} from './location.model';
import {IRate} from './interfaces/IRate';
import {WorkingHours, StoreTime} from './working.hours.model';
import {Seo} from './seo';
import {ICity} from './interfaces/ICity';

export class Store implements IStore {
    name: ITranslate;
    pic: Array<string>;
    city: ICity | string;
    link: string;
    ind: number;
    toHomePage: boolean;
    address: ITranslate;
    tel: string;
    fax: string;
    mobile: string;
    mail: string;
    description: ITranslate;
    adminActive: Boolean;
    active: Boolean;
    callToStore: Boolean;
    callNumber: Boolean;
    message: string;
    adminMessage: string;
    storeLocation: Location;
    payments: Array<PaymentType>;
    deliveryTypes: Array<DeliveryType>;
    //form added for now
    items: Array<ItemStoreOffer>;
    delivery: Array<Delivery>;
    // deliveryPlaces: Array<Delivery>;
    packages: Array<PackageStoreOffer>;
    rate: Array<IRate>;
    // added to form
    weekHours: WorkingHours;
    weekEndHours: WorkingHours;
    shabatHours: WorkingHours;
    seo: Seo;
    _id?: string;
    /**
     * Constructor
     *
     * @param item
     */
    constructor(item) {
        {
            this.name = item.name || {en: '', heb: ''};
            this.pic = item.pic || '';
            this.link = item.link;
            this.ind = item.ind;
            this.toHomePage = item.toHomePage || false;
            this.city = item.city || '';
            this.address = item.address || {en: '', heb: ''};
            this.tel = item.tel || '';
            this.fax = item.fax || '';
            this.mobile = item.mobile || '';
            this.mail = item.mail || '';
            this.description = item.description || {en: '', heb: ''};
            // что это такое
            this.adminActive = item.adminActive || false;
            this.active = item.active || false;
            this.callToStore = item.callToStore || false;
            this.callNumber = item.callNumber || false;
            // ????
            this.message = item.message || '';
            this.adminMessage = item.adminMessage || '';
            this.payments = item.payments || [PaymentType.phone];
            this.deliveryTypes = item.deliveryTypes || [DeliveryType.delivery];
            this.items = item.items || [];
            this.delivery = item.delivery || [];
            // deliveryPlaces: Array<Delivery>;
            this.packages = item.packages || [];
            this.storeLocation = item.storeLocation || new Location();
            this.rate = item.rate || [];
            this.weekHours = item.weekHours || new WorkingHours(true, new StoreTime(8, 0), new StoreTime(18, 0));
            this.weekEndHours = item.weekEndHours || new WorkingHours(true, new StoreTime(8, 0), new StoreTime(18, 0));
            this.shabatHours = item.shabatHours || new WorkingHours(true, new StoreTime(8, 0), new StoreTime(18, 0));
            this._id = item._id || '';
        }
    }
}
