import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
    MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule, MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatTableModule, MatToolbarModule,
    MatSelectModule, MatSortModule
} from '@angular/material';

import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';

import { CityComponent } from './city.component';
import {CityService} from '../../../bussiness/services/city.service';
import {CountryService} from '../../../bussiness/services/country.service';
import {RegionService} from '../../../bussiness/services/region.service';
import { CityModuleService } from './city.module.service';
import {CitiesCityListComponent} from './city-list/city-list.component';
import {CountriesCountryFormDialogComponent} from './forms/country-form/country-form.component';
import {RegionsRegionFormDialogComponent} from './forms/region-form/region-form.component';
import {CitiesCityFormDialogComponent} from './forms/city-form/city-form.component';
import { FuseConfirmDialogModule, FuseSidebarModule } from '@fuse/components';
import {HashveModule} from 'app/components/hashve.module';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { FileUploadModule } from 'ng2-file-upload';

const routes: Routes = [
    {
        path     : '**',
        component: CityComponent,
        resolve  : {
            cities: CityModuleService
        }
    } 
];

@NgModule({
    declarations: [
        CityComponent,
        CitiesCityListComponent,
        CountriesCountryFormDialogComponent,
        RegionsRegionFormDialogComponent,
        CitiesCityFormDialogComponent
    ],
    imports     : [
        AngularEditorModule,
        RouterModule.forChild(routes),
        HashveModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatTableModule,
        MatToolbarModule,
        MatSelectModule,
        MatSortModule,

        TranslateModule,
        FileUploadModule,
        FuseConfirmDialogModule,
        FuseSharedModule
    ],
    providers: [
        CityService,
        CountryService,
        RegionService,
        CityModuleService
    ],
    exports     : [
        CityComponent
    ],
    entryComponents: [
        CountriesCountryFormDialogComponent,
        RegionsRegionFormDialogComponent,
        CitiesCityFormDialogComponent
    ]
})

export class CityModule
{
}
