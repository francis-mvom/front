export const environment = {
    production: false,
    apihost: 'http://localhost:3000',
    userEndpoint: '/api/v1/user',
    cityEndpoint: '/api/v1/city',
    countryEndpoint: '/api/v1/country',
    regionEndpoint: '/api/v1/region',
    itemEndpoint: '/api/v1/item',
    categoryEndpoint: '/api/v1/category',
    storeEndpoint: '/api/v1/store',
    packageEndpoint: '/api/v1/package',
    uploadFile: '/api/v1/upload',
    uploadCategory: '/category',
    uploadItem: '/item',
    uploadRegion: '/region',
    uploadStore: '/store',
    hmr       : true
};
