import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {
    ActivatedRouteSnapshot,
    Resolve,
    RouterStateSnapshot
} from '@angular/router';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {FuseUtils} from '@fuse/utils';

import {Category} from 'app/bussiness/model/category.model';
import {Item} from 'app/bussiness/model/item.model';
import {ItemService} from 'app/bussiness/services/item.service';
import {CategoryService} from 'app/bussiness/services/category.service';

@Injectable()
export class ItemModuleService implements Resolve<any> {
    onCategoriesChanged: BehaviorSubject<any>;
    onItemsChanged: BehaviorSubject<any>;

    onSelectedContactsChanged: BehaviorSubject<any>;
    onUserDataChanged: BehaviorSubject<any>;
    onSearchTextChanged: Subject<any>;
    onFilterChanged: Subject<any>;

    items: Item[];
    categories: Category[];

    user: any;
    selectedContacts: string[] = [];
    category: Category;
    searchText: string;
    filterBy: string;

    /**
     * Constructor
     *
     * @param {RegionService} regionService
     * @param {CityService} cityService
     * @param {CountryService} countryService
     */
    constructor(
        private itemService: ItemService,
        private categoryService: CategoryService,
    ) {
        // Set the defaults
        this.onItemsChanged = new BehaviorSubject([]);
        this.onCategoriesChanged = new BehaviorSubject([]);

        /*this.onSelectedContactsChanged = new BehaviorSubject([]);
        this.onUserDataChanged = new BehaviorSubject([]);
        this.onSearchTextChanged = new Subject();*/
        this.onFilterChanged = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {
            Promise.all([this.getItems(), this.getCategories()]).then(
                ([files]) => {
                    /*this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getContacts();
                    });*/

                    this.onFilterChanged.subscribe(filter => {
                        this.category = filter;
                    });
                    resolve(this.items);
                }, reject);
        });
    }

    /**
     * Get items
     *
     * @returns {Promise<any>}
     */
    getItems(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.itemService.list().subscribe(data => {
                this.items = data.map(item => {
                    return new Item(item);
                });
                this.onItemsChanged.next(this.items);
                resolve(data);
            });
        });
    }

    getCategories(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.categoryService.list().subscribe(data => {
                this.categories = data.map(item => {
                    return new Category(item);
                });
                this.onCategoriesChanged.next(this.categories);
                resolve(data);
            });
        });
    }

    /**
     * Toggle selected contact by id
     *
     * @param id
     */
    /*toggleSelectedContact(id): void {
        // First, check if we already have that contact as selected...
        if (this.selectedContacts.length > 0) {
            const index = this.selectedContacts.indexOf(id);

            if (index !== -1) {
                this.selectedContacts.splice(index, 1);

                // Trigger the next event
                this.onSelectedContactsChanged.next(this.selectedContacts);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedContacts.push(id);

        // Trigger the next event
        this.onSelectedContactsChanged.next(this.selectedContacts);
    }*/

    /**
     * Toggle select all
     */
    /*toggleSelectAll(): void {
        if (this.selectedContacts.length > 0) {
            this.deselectContacts();
        } else {
            this.selectContacts();
        }
    }*/

    /**
     * Select contacts
     *
     * @param filterParameter
     * @param filterValue
     */

    /*selectContacts(filterParameter?, filterValue?): void {
        this.selectedContacts = [];

        // If there is no filter, select all contacts
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedContacts = [];
            this.contacts.map(contact => {
                this.selectedContacts.push(contact._id);
            });
        }

        // Trigger the next event
        this.onSelectedContactsChanged.next(this.selectedContacts);
    }*/

    /**
     * Update country
     *
     * @param category
     * @returns {Promise<any>}
     */
    updateCategory(id, country): Promise<any> {
        if (id === null) {
            return new Promise((resolve, reject) => {
                this.categoryService.create(country).subscribe(response => {
                    this.getCategories();
                    resolve(response);
                });
            });
        } else {
            country._id = id;
            return new Promise((resolve, reject) => {
                this.categoryService.update(country).subscribe(response => {
                    this.getCategories();
                    resolve(response);
                });
            });
        }
    }

    updateItem(id, item): Promise<any> {
        if (id === null) {
            return new Promise((resolve, reject) => {
                this.itemService.create(item).subscribe(response => {
                    this.getItems();
                    resolve(response);
                });
            });
        } else {
            item._id = id;
            return new Promise((resolve, reject) => {
                this.itemService.update(item).subscribe(response => {
                    this.getItems();
                    resolve(response);
                });
            });
        }
    }

    /**
     * Update user data
     *
     * @param userData
     * @returns {Promise<any>}
     */
    /*updateUserData(userData): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient
                .post("api/contacts-user/" + this.user.id, { ...userData })
                .subscribe(response => {
                    this.getUserData();
                    this.getContacts();
                    resolve(response);
                });
        });
    }*/

    /**
     * Deselect contacts
     */

    /*deselectContacts(): void {
        this.selectedContacts = [];

        // Trigger the next event
        this.onSelectedContactsChanged.next(this.selectedContacts);
    }*/

    /**
     * Delete item
     *
     * @param item
     */
    deleteItem(item): void {
        this.itemService.delete(item._id).subscribe(data => {
            const contactIndex = this.items.indexOf(item);
            this.items.splice(contactIndex, 1);
            this.onItemsChanged.next(this.items);
        });
    }

    /**
     * Delete item
     *
     * @param item
     */
    deleteCategory(category): void {
        this.categoryService.delete(category._id).subscribe(data => {
            const categoryIndex = this.categories.indexOf(category);
            this.categories.splice(categoryIndex, 1);
            this.onCategoriesChanged.next(this.categories);
        });
    }

    /**
     * Delete city
     *
     * @param country
     */
    /*deleteCity(city): void {
        this.cityService.delete(city._id).subscribe(data => {
            const cityIndex = this.cities.indexOf(city);
            this.cities.splice(cityIndex, 1);
            this.onCitiesChanged.next(this.cities);
        });
    }*/

    /**
     * Delete selected contacts
     */
    /*deleteSelectedContacts(): void {
        for (const contactId of this.selectedContacts) {
            const contact = this.contacts.find(_contact => {
                return _contact._id === contactId;
            });
            const contactIndex = this.contacts.indexOf(contact);
            this.contacts.splice(contactIndex, 1);
            this.userService.delete(contact._id).subscribe(data => {});
        }
        this.onContactsChanged.next(this.contacts);
        this.deselectContacts();
    }*/
}
