export const locale = {
    lang: 'en',
    data: {
        'NAV': {
            'APPLICATIONS': 'Applications',
            'SAMPLE'        : {
                'TITLE': 'Sample',
                'BADGE': '25'
            },
            'CONTACTS'    : 'Contacts',
            'CITY'        : 'City',
            'ITEM'        : 'Item',
            'PACKAGE'     : 'Package',
            'STORE'       : 'Store',
            'ORDER'       : 'Order',
            'ARTICLE'       : 'Articles',
            'MESSAGE'       : 'Message'
        }
    }
};
