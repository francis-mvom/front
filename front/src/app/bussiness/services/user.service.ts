import {Injectable, EventEmitter} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from './base/service.base';
import {IUser} from '../model/interfaces/IUser';
import {environment} from '../../../environments/environment';

@Injectable()
export class UserService extends BaseService<IUser> {

    constructor(private http: HttpClient) {
        super(http, environment.apihost, environment.userEndpoint);
    }

    login(mail: string, password: string){
        return this.httpClient.post(`${this.url}${this.endpoint}/login`, {mail: mail, password: password});
    }
}
