import {Injectable, OnInit} from '@angular/core';
import {AuthenticationService} from '../../bussiness/services/authentication.service';
import {Observable} from 'rxjs';
import {AppState, selectAuthenticationState} from '../../bussiness/store/app.state';
import {Logout} from '../../bussiness/store/actions/authentication.actions';
import {Store} from '@ngrx/store';

@Injectable()
export class LoginModuleService implements OnInit {

    getState: Observable<any>;
    isAuthenticated: false;
    user = null;
    errorMessage = null;
    constructor(
        private store: Store<AppState>
    ) {
        this.getState = this.store.select(selectAuthenticationState);
    }
ngOnInit() {
    this.getState.subscribe((state) => {
        this.isAuthenticated = state.isAuthenticated;
        this.user = state.user;
        this.errorMessage = state.errorMessage;
    });
}

logOut(): void {
    this.store.dispatch(new Logout);
}
}
