import {Component, ViewEncapsulation} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

import {MatDialog} from '@angular/material';

import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {fuseAnimations} from '@fuse/animations';

import {ItemModuleService} from './item.module.service';

import {locale as english} from './i18n/en';
import {locale as turkish} from './i18n/tr';
import {CategoriesCategoryFormDialogComponent} from './forms/category-form/category-form.component';
import {ItemsItemFormDialogComponent} from './forms/item-form/item-form.component';
import {Category} from '../../../bussiness/model/category.model';
import {Item} from '../../../bussiness/model/item.model';

@Component({
    selector: 'city',
    templateUrl: './item.component.html',
    styleUrls: ['./item.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ItemComponent {
    dialogRef: any;
    hasSelectedContacts: boolean;
    searchInput: FormControl;
    items: Array<Item>;
    categories: Array<Category>;

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private itemModuleService: ItemModuleService,
        private _fuseSidebarService: FuseSidebarService,
        private _matDialog: MatDialog
    ) {
        this._fuseTranslationLoaderService.loadTranslations(english, turkish);
        this.itemModuleService.onItemsChanged.subscribe(data => {
            this.items = data;
        });
        this.itemModuleService.onCategoriesChanged.subscribe(data => {
            this.categories = data;
        });
    }

    /*deleteItem(clientValue: Country): void {
        this.itemModuleService.deleteCountry(clientValue);
    }*/

    public newItem(): void {
        this.dialogRef = this._matDialog.open(ItemsItemFormDialogComponent, {
            panelClass: 'contact-form-dialog',
            data: {
                action: 'new',
                categories: this.categories
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                const newItem: Item = new Item({});
                newItem.name.en = response.value.name_en;
                newItem.name.heb = response.value.name_heb;
                newItem.description.en = response.value.description_en;
                newItem.description.heb = response.value.description_heb;
                newItem.code = response.value.code;
                newItem.rate = response.value.rate;
                newItem.picture = response.value.picture === '' ||
                    response.value.picture === undefined ||
                    response.value.picture === null ? 'no-photo-available.png' : response.value.picture;
                newItem.additional = response.value.additional;
                newItem.recommended = response.value.recommended;
                newItem.rate = [];
                newItem.category = response.value.category;
                newItem.dayDeal = response.value.deal;
                newItem.inStock = response.value.inStock;
                newItem.price = response.value.price;
                newItem.seo = response.value.seo;

                this.itemModuleService.updateItem(null, newItem);
            });
    }

    public editItem(): void {
        this.dialogRef = this._matDialog.open(CategoriesCategoryFormDialogComponent, {
            panelClass: 'contact-form-dialog',
            data: {
                action: 'new',
                categories: this.categories
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                const newCategory: Category = new Category({});
                /*newCategory.name.en = response.value.name_en;
                newCategory.name.heb = response.value.name_heb;
                newCategory.tagline.en = response.value.tagline_en;
                newCategory.tagline.heb = response.value.tagline_heb;
                this.itemModuleService.updateCategory(null, newCategory);*/
            });
    }

    public newCategory(): void {
        this.dialogRef = this._matDialog.open(CategoriesCategoryFormDialogComponent, {
            panelClass: 'contact-form-dialog',
            data: {
                action: 'new',
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                const newCategory: Category = new Category({});
                newCategory.name.en = response.value.name_en;
                newCategory.name.heb = response.value.name_heb;
                newCategory.tagline.en = response.value.tagline_en;
                newCategory.tagline.heb = response.value.tagline_heb;
                newCategory.description.heb = response.value.description_heb;
                newCategory.description.en = response.value.description_en;
                newCategory.renderOnHomePage = response.value.renderOnHomePage;
                newCategory.disabled = response.value.disabled;
                newCategory.toTopMenu = response.value.toTopMenu;
                newCategory.toFooterMenu = response.value.toFooterMenu;
                newCategory.subcategory = response.value.subcategory;
                newCategory.seo = response.value.seo;
                newCategory.pic =  response.value.pic === '' ||
                    response.value.pic === undefined ||
                    response.value.pic === null ? 'no-photo-available.png' : response.value.pic;
                this.itemModuleService.updateCategory(null, newCategory);
            });
    }

    editCategory(clientValue: Category): void {
        this.dialogRef = this._matDialog.open(CategoriesCategoryFormDialogComponent, {
            panelClass: 'contact-form-dialog',
            data: {
                action: 'edit',
                category: clientValue,
            }
        });
        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                const newCategory: Category = new Category({});
                newCategory._id = clientValue._id;
                newCategory.name.en = response[1].value.name_en;
                newCategory.name.heb = response[1].value.name_heb;
                newCategory.description.heb = response[1].value.description_heb;
                newCategory.description.en = response[1].value.description_en;
                newCategory.tagline.en = response[1].value.tagline_en;
                newCategory.tagline.heb = response[1].value.tagline_heb;
                newCategory.renderOnHomePage = response[1].value.renderOnHomePage;
                newCategory.disabled = response[1].value.disabled;
                newCategory.toFooterMenu = response[1].value.toFooterMenu;
                newCategory.subcategory = response[1].value.subcategory;
                newCategory.toTopMenu = response[1].value.toTopMenu;
                newCategory.seo = response[1].value.seo;
                newCategory.pic = response[1].value.pic === '' ||
                    response[1].value.pic === undefined ||
                    response[1].value.pic === null ? 'no-photo-available.png' : response[1].value.pic;
                newCategory.items = clientValue.items;
                this.itemModuleService.updateCategory(clientValue._id, newCategory);
            });
    }

    deleteCategory(category: Category): void{
        this.itemModuleService.deleteCategory(category);
    }

    /**
     * Toggle the sidebar
     *
     * @param name
     */
    toggleSidebar(name): void
    {
        this._fuseSidebarService.getSidebar(name).toggleOpen();
    }
}
