import {Component, Inject, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, FormControl} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {environment} from '../../../../../environments/environment';
import * as _ from 'lodash';
import {AngularEditorConfig} from '@kolkov/angular-editor';
import {ArticleModuleService} from '../article.module.service';
import {Article} from '../../../../bussiness/model/article.model';
import {ArticleType} from '../../../../bussiness/model/Enum/ArticleType';
import {BindingType} from '../../../../bussiness/model/Enum/BindingType';
import {CityService} from '../../../../bussiness/services/city.service';
import {RegionService} from '../../../../bussiness/services/region.service';
import {City} from 'app/bussiness/model/city.model';
import {Region} from '../../../../bussiness/model/region.model';
import {toPromise} from 'rxjs-compat/operator/toPromise';
import {Observable} from 'rxjs';
import {ICity} from 'app/bussiness/model/interfaces/ICity';
import {FileUploadService} from '../../../../bussiness/services/file-upload.service';


@Component({
    selector: 'article-form-dialog',
    templateUrl: './article-form.component.html',
    styleUrls: ['./article-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ArticleFormDialogComponent {
    editorConfig: AngularEditorConfig = {
        editable: true,
        spellcheck: true,
        height: '25rem',
        minHeight: '5rem',
        placeholder: 'Enter text here...',
        translate: 'no',
        uploadUrl: 'v1/images', // if needed
        customClasses: [ // optional
            {
                name: 'quote',
                class: 'quote',
            },
            {
                name: 'redText',
                class: 'redText'
            },
            {
                name: 'titleText',
                class: 'titleText',
                tag: 'h1',
            },
        ]
    };
    _articleType = ArticleType;
    _bindingType = BindingType;
    action: string;
    article: Article;
    articleForm: FormGroup;
    dialogTitle: string;
    apiHost: string = environment.apihost;
    cities: Observable<Array<ICity>>;
    regions: Observable<Array<Region>>;
    itemPicPath: string = '/assets/articles/';
    picPath: string = environment.apihost + environment.uploadFile + environment.uploadArticle;

    /**
     * Constructor
     *
     * @param {MatDialogRef<CitiesCityFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<ArticleFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder,
        private articleModuleService: ArticleModuleService,
        private cityService: CityService,
        private regionService: RegionService,
        private fileUploadService: FileUploadService,
    ) {
        // Set the defaults
        this.action = _data.action;
        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Article';
            this.article = _data.article;
            /*this.selectedRegions = _.filter(this.regions, (o) => {
                return o.country === this.city.country;
            });*/
        } else {
            this.dialogTitle = 'New Article';
            this.article = new Article({});
        }
        this.fileUploadService.onPhotoUploaded.subscribe(data => {
            debugger;
            this.articleForm.get('pic').setValue(data);
        });
        this.articleForm = this.createArticleForm();
        this.cities = this.cityService.list().map((data) => {
            data.sort((a, b) => {
                return a.name.heb < b.name.heb ? -1 : 1;
            });
            return data;
        });

        this.regions = this.regionService.list().map((data) => {
            data.sort((a, b) => {
                return a.name.heb < b.name.heb ? -1 : 1;
            });
            return data;
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create contact form
     *
     * @returns {FormGroup}
     */
    createArticleForm(): FormGroup {
        return this._formBuilder.group({
            _id: [this.article._id],
            header_en: [this.article.header.en],
            header_heb: [this.article.header.heb],
            slogan_heb: [this.article.slogan.heb],
            slogan_en: [this.article.slogan.en],
            shitFromLeft_heb: [this.article.shitFromLeft.heb],
            shitFromLeft_en: [this.article.shitFromLeft.en],
            text_heb: [this.article.text.heb],
            text_en: [this.article.text.en],
            pic: [this.article.pic],
            showInList: [this.article.showInList],
            showOnHomePage: [this.article.showOnHomePage],
            articleType: [this.article.articleType],
            binding: this._formBuilder.group({
                bindingType: [this.article.binding.bindingType],
                object: [this.article.binding.object]
            }),
            // object: [this.article.object],
            seo: new FormControl(this.article.seo)
        });
    }

    /*onCountrySelect(stateName): void{
        this.selectedRegions = _.filter(this.regions, function(o) {
            return o.country === stateName;
        });
    }*/
}
