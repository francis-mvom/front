import {Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation, Input} from '@angular/core';
import {MatDialog, MatDialogRef, MatSort} from '@angular/material';
import {DataSource} from '@angular/cdk/collections';
import {merge, Observable, Subject} from 'rxjs';
import {map, takeUntil} from 'rxjs/operators';
import {environment} from '../../../../../environments/environment';
import {fuseAnimations} from '@fuse/animations';
// import {FuseConfirmDialogComponent} from '@fuse/components/confirm-dialog/confirm-dialog.component';

import {Order} from '../../../../bussiness/model/order.model';
import {OrderModuleService} from '../order.module.service';
import {PaymentType} from '../../../../bussiness/model/Enum/PaymentType';
import {StatusType} from '../../../../bussiness/model/Enum/StatusType';
import {DeliveryType} from '../../../../bussiness/model/Enum/DeliveryType';

@Component({
    selector: 'order-contact-list',
    templateUrl: './order-list.component.html',
    styleUrls: ['./order-list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class OrdersOrderListComponent implements OnInit, OnDestroy {
    @ViewChild('dialogContent')
    dialogContent: TemplateRef<any>;
    @ViewChild(MatSort) sort: MatSort;
    _deliveryType: any = DeliveryType;
    /*@Input()
    set region(val: Region) {
        this._citiesService.onFilterChanged.next(val);
    }*/

    orders: Array<Order> = [];
    apiHost: string = environment.apihost;
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'order_number','sender_name','date','store_name','products','delivery_city','payment_method', 'full_price' , 'status',  'buttons'];
    selectedContacts: any[];
    checkboxes: {};
    dialogRef: any;
    _payementType : any = PaymentType;
    orderStatuses: any = StatusType;
    /*confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;*/

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ContactsService} _contactsService
     * @param {MatDialog} _matDialog
     */

    constructor(
        private _orderService: OrderModuleService,
        public _matDialog: MatDialog
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.dataSource = new FilesDataSource(this._orderService, this.sort);
        this._orderService.onOrdersChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(orders => {
                this.orders = orders;
                this.checkboxes = {};
                orders.map(order => {
                    this.checkboxes[order._id] = false;
                });
            });
        /*this._citiesService.onSelectedCityChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(selectedContacts => {
                for ( const id in this.checkboxes )
                {
                    if ( !this.checkboxes.hasOwnProperty(id) )
                    {
                        continue;
                    }

                    this.checkboxes[id] = selectedContacts.includes(id);
                }
                this.selectedContacts = selectedContacts;
            });*/

        /*this._contactsService.onUserDataChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(user => {
                this.user = user;
            });*/

        /*this._contactsService.onFilterChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this._contactsService.deselectContacts();
            });*/
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Edit contact
     *
     * @param contact
     */
/*    editContact(city): void {
        this.dialogRef = this._matDialog.open(CitiesCityFormDialogComponent, {
            panelClass: 'contact-form-dialog',
            data: {
                city: city,
                countries: this.countries,
                regions: this.regions,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /!**
                     * Save
                     *!/
                    case 'save':
                        const newCity: City = new City({});
                        newCity.name.en = formData.value.name_en;
                        newCity.name.heb = formData.value.name_heb;
                        newCity.description.en = formData.value.description_en;
                        newCity.description.heb = formData.value.description_heb;
                        newCity.country = formData.value.country;
                        newCity.region = formData.value.region;
                        newCity.seo = formData.value.seo;
                        this._citiesService.updateCity(city._id, newCity);

                        break;
                    /!**
                     * Delete
                     *!/
                    case 'delete':

                        this.deleteCity(city);

                        break;
                }
            });
    }*/

    /*/!**
     * Delete City
     *!/*/
    /*deleteCity(city): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this._citiesService.deleteCity(city);
            }
            this.confirmDialogRef = null;
        });

    }*/

    /*
        /**
         * On selected change
         *
         * @param contactId
         */
    onSelectedChange(contactId): void
    {
        this._orderService.toggleSelectedOrders(contactId);
    }

    /**
     * set order in selectedOrderChange
     *
     * @param order
     */
    onClickOrder(order): void
    {
        this._orderService.toggleSelectOneOrder(order);
    }


    /**
     * Toggle star
     *
     * @param contactId
     */
    /*toggleStar(contactId): void
    {
        if ( this.user.starred.includes(contactId) )
        {
            this.user.starred.splice(this.user.starred.indexOf(contactId), 1);
        }
        else
        {
            this.user.starred.push(contactId);
        }

        this._contactsService.updateUserData(this.user);
    }*/
}

export class FilesDataSource extends DataSource<any> {
    /**
     * Constructor
     *
     * @param {CityModuleService} _citiesService
     */
    constructor(
        private _ordersService: OrderModuleService,
        private _matSort: MatSort
    ) {
        super();
    }

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            this._ordersService.onOrdersChanged,
            // this._matPaginator.page,
            this._ordersService.onFilterChanged,
            this._ordersService.onFilterByStoreChanged,
            this._matSort.sortChange
        ];

        return merge(...displayDataChanges).pipe(
            map(() => {
                let data = this._ordersService.orders.slice();
                // data = data.filter(item => this._ordersService.region ? this._citiesService.region._id === item.region : true);

                const fiType = this._ordersService.filterByStore;
                data = data.filter(item =>fiType.length===0 ? true : fiType.includes(item.productOffer.store._id));
                data = this.sortData(data);
                return data.sort((a, b) => {
                    let propertyA:  String = '';
                    let propertyB:  String = '';
                     [propertyA, propertyB] = [a.productOffer.store.name.heb, b.productOffer.store.name.heb];
                    const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
                    const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
                    return (valueA < valueB ? -1 : 1) ;
                });
            })
        );
    }

    /**
     * Sort data
     *
     * @param data
     * @returns {any[]}
     */
    sortData(data): any[] {
        if (!this._matSort.active || this._matSort.direction === '') {
            return data;
        }

        return data.sort((a, b) => {
            let propertyA: number | string = '';
            let propertyB: number | string = '';
            switch (this._matSort.active) {
                case 'store_name_heb':
                    [propertyA, propertyB] = [a.productOffer.store.name.heb, b.productOffer.store.name.heb];
                    break;
                case 'discount_type':
                    [propertyA, propertyB] = [a.typeOfDiscount, b.typeOfDiscount];
                    break;
                case 'delivery_price':
                    [propertyA, propertyB] = [a.deliveryPrice, b.deliveryPrice];
                    break;
                case 'price':
                    [propertyA, propertyB] = [a.price, b.price];
                    break;
            }

            const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

            return (valueA < valueB ? -1 : 1) * (this._matSort.direction === 'asc' ? 1 : -1);
        });
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}
