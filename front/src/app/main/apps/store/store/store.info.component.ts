import {Component, OnDestroy, OnInit, ViewEncapsulation, Input, ViewChild, ElementRef} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import {Subscription} from 'rxjs';
import {fuseAnimations} from '@fuse/animations';
import {Router} from '@angular/router';
import * as _ from 'lodash';
import {environment} from 'environments/environment';
import {StoreComponentService} from './store.component.service';
import {Rate} from 'app/bussiness/model/rate.model';
import {FuseReviewFormDialogComponent} from '../review-form/review-form.component';
import {IStore} from 'app/bussiness/model/interfaces/IStore';
import {Store} from '../../../../bussiness/model/store.model';
import {IPackage} from '../../../../bussiness/model/interfaces/IPackage';
import {Package} from '../../../../bussiness/model/package.model';
import {IItem} from '../../../../bussiness/model/interfaces/IItem';
import {Item} from 'app/bussiness/model/item.model';
import {ICity} from 'app/bussiness/model/interfaces/ICity';
import {City} from 'app/bussiness/model/city.model';
import {DeliveryType} from 'app/bussiness/model/Enum/DeliveryType';
import {PaymentType} from 'app/bussiness/model/Enum/PaymentType';
import {TranslateFormGroupBuilder} from '../../../../bussiness/model/FormGroupBuilders/TranslateFormGroupBuilder';
import {FileUploadService} from 'app/bussiness/services/file-upload.service';
import {ItemSize} from '../../../../bussiness/model/Enum/ItemSize';
import {PackageStoreOffer} from '../../../../bussiness/model/package.store.offer';
import {AngularEditorConfig} from '@kolkov/angular-editor';
import {FileUploader} from 'ng2-file-upload';

@Component({
    selector: 'fuse-e-commerce-product',
    templateUrl: './store.info.component.html',
    styleUrls: ['./store.info.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class FuseStoreInfoComponent implements OnInit, OnDestroy {
    editorConfig: AngularEditorConfig = {
        editable: true,
        spellcheck: true,
        height: '25rem',
        minHeight: '5rem',
        placeholder: 'Enter text here...',
        translate: 'no',
        uploadUrl: 'v1/images', // if needed
        customClasses: [ // optional
            {
                name: 'quote',
                class: 'quote',
            },
            {
                name: 'redText',
                class: 'redText'
            },
            {
                name: 'titleText',
                class: 'titleText',
                tag: 'h1',
            },
        ]
    };
    onStoreChanged: Subscription;
    onCityChanged: Subscription;
    onPackagesChanged: Subscription;
    onItemsChanged: Subscription;
    paymentType = PaymentType.values();
    paymenttype = PaymentType;
    ItemSize = ItemSize;
    deliveryType = DeliveryType.values();
    deliverytype = DeliveryType;
    editedReview: Rate;

    picPath: string =
        environment.apihost + environment.uploadFile + environment.uploadStore;
    public uploader: FileUploader = new FileUploader({ url: environment.apihost + environment.uploadFile + environment.uploadStore, itemAlias: 'file' });
    apiHost: string = environment.apihost;

    pageType: string;
    langs: string[];
    store: Store;
    cities: Array<ICity>;
    packages: Array<IPackage>;
    items: Array<IItem>;
    itemsList: Array<any> = [];
    storeForm: FormGroup;
    serverPath: string;

    storeAvatar: string;
    storePhoto: Array<string>;

    file: File;
    files: File[] | File;
    hours: number[] = [];
    minutes: number[] = [];
    disabled: boolean = false;
    packCollection: PackageStoreOffer[] = [];
    reviewDialogRef: any;

    selectedCity: ICity;
    selectedPrice: number;

    constructor(
        private storeService: StoreComponentService,
        private formBuilder: FormBuilder,
        public snackBar: MatSnackBar,
        public dialog: MatDialog,
        private router: Router,
        private fileUploadService: FileUploadService
    ) {
        this.serverPath = environment.apihost;
        for (let i = 0; i < 60; i++) {
            if (i < 24) {
                this.hours.push(i);
            }
            this.minutes.push(i);
        }
    }

    ngOnInit() {
        // Subscribe to update product on changes
        this.onStoreChanged = this.storeService.onStoreChanged.subscribe(
            store => {
                if (store) {
                    this.store = store;
                    this.storeAvatar =
                        this.store.pic === undefined ||
                        this.store.pic[0] === undefined
                            ? 'no_avatar.jpg'
                            : this.store.pic[0];
                    this.storePhoto = this.store.pic;
                    this.pageType = 'edit';
                } else {
                    this.pageType = 'new';
                    this.store = new Store({});
                }
                this.storeForm = this.createStoreForm();
            }
        );

        /*this.fileUploadService.onPhotoUploaded.subscribe(data => {
            const pictures: string[] = [];
            if (typeof data === 'string') {
                pictures.push(data);
            } else {
                for (const name in data) {
                    if (data[name].name !== undefined) {
                        pictures.push(data[name].name);
                    }
                }
            }
            pictures.forEach(element => {
                this.store.pic.push(element);
                // this.
            });
        });*/

        this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            // console.log('FileUpload:uploaded successfully:', item, status, response);
            this.store.pic.push(JSON.parse(response).data[0].name);
            alert('Your file has been uploaded successfully');
        };

        this.onCityChanged = this.storeService.onCityChanged.subscribe(city => {
            if (city) {
                this.cities = city;
                /*if(this.store.city){
                    let index = _.findIndex(this.cities, city => city._id === this.store.city);
                    let c = this.cities[index];
                    this.cities = this.cities.filter((city, index) => city.region === c.region);
                }*/
            } else {
                this.cities = new Array<City>();
            }
        });
        this.onItemsChanged = this.storeService.onItemsChanged.subscribe(
            items => {
                if (items) {
                    this.items = items;
                    this.items.forEach((item, index) => {
                        const curItem = _.find(this.store.items, (value) => {
                            return value.item === item._id;
                        });
                        if (curItem) {
                            this.itemsList.push({
                                item: item,
                                price: curItem.price,
                                discount: curItem.discount
                            });
                        } else {
                            this.itemsList.push({
                                item: item,
                                price: 0,
                                discount: 0
                            });
                        }
                    });
                } else {
                    this.items = new Array<Item>();
                }
            }
        );
        this.onPackagesChanged = this.storeService.onPackagesChanged.subscribe(
            pack => {
                if (pack) {
                    this.packages = pack;
                    this.packages.forEach(pack => {
                        const index = _.findIndex(this.store.packages, item => item.package === pack._id);
                        let item: PackageStoreOffer = new PackageStoreOffer();
                        if (index > -1) {
                            item.package = pack;
                            item.price = this.store.packages[index].price;
                        } else {
                            item.package = pack;
                            item.price = -1;
                        }
                        this.packCollection.push(item);
                    });
                    console.log(this.packCollection);
                } else {
                    this.packages = new Array<Package>();
                }
            }
        );
    }

    ngOnDestroy() {
        this.onStoreChanged.unsubscribe();
        this.onPackagesChanged.unsubscribe();
        this.onCityChanged.unsubscribe();
        this.onItemsChanged.unsubscribe();
    }

    addItem(pack, $event, price, index): void {
        if (this.store.packages === undefined) {
            this.store.packages = [];
        }
        if ($event.checked) {
            this.store.packages.push({package: pack._id, price: price});
            console.log(this.store.packages);
        } else {
            let index = _.findIndex(this.store.packages, (item) => item.package === pack._id);
            if (index > -1) {
                this.store.packages.splice(index, 1);
                console.log(this.store.packages);
            }
        }
    }

    createStoreForm() {
        let translateBuilder = new TranslateFormGroupBuilder();
        return this.formBuilder.group({
            _id: [this.store._id],
            name: translateBuilder.buildGroup(this.store.name),
            city: [this.store.city],
            link: [this.store.link],
            ind: [this.store.ind],
            toHomePage: [this.store.toHomePage],
            address: translateBuilder.buildGroup(this.store.address),
            description: translateBuilder.buildGroup(this.store.description),
            tel: [this.store.tel],
            fax: [this.store.fax],
            //pic: [this.store.pic],
            mobile: [this.store.mobile],
            mail: [this.store.mail],
            adminActive: [this.store.adminActive],
            active: [this.store.active],
            callToStore: [this.store.callToStore],
            callNumber: [this.store.callNumber],
            message: [this.store.message],
            adminMessage: [this.store.adminMessage],
            storeLocation: this.formBuilder.group({
                lat: [this.store.storeLocation.lat],
                lng: [this.store.storeLocation.lng]
            }),
            payments: [this.store.payments],
            deliveryTypes: [this.store.deliveryTypes],
            weekHours: this.formBuilder.group({
                active: [this.store.weekHours.active],
                open: this.formBuilder.group({
                    hour: [this.store.weekHours.open.hour],
                    minute: [this.store.weekHours.open.minute]
                }),
                close: this.formBuilder.group({
                    hour: [this.store.weekHours.close.hour],
                    minute: [this.store.weekHours.close.minute]
                })
            }),
            weekEndHours: this.formBuilder.group({
                active: [this.store.weekEndHours.active],
                open: this.formBuilder.group({
                    hour: [this.store.weekEndHours.open.hour],
                    minute: [this.store.weekEndHours.open.minute]
                }),
                close: this.formBuilder.group({
                    hour: [this.store.weekEndHours.close.hour],
                    minute: [this.store.weekEndHours.close.minute]
                })
            }),
            shabatHours: this.formBuilder.group({
                active: [this.store.shabatHours.active],
                open: this.formBuilder.group({
                    hour: [this.store.shabatHours.open.hour],
                    minute: [this.store.shabatHours.open.minute]
                }),
                close: this.formBuilder.group({
                    hour: [this.store.shabatHours.close.hour],
                    minute: [this.store.shabatHours.close.minute]
                })
            }),
            seo: new FormControl(this.store.seo)
            /*
            active: [this.store.active],
            priceDeviation: [this.store.priceDeviation],*/
        });
    }

    saveStore(): void {
        const data = this.storeForm.getRawValue();
        data.pic = this.store.pic;
        data.delivery = this.store.delivery;
        data.packages = [];
        this.packCollection.forEach(item => {
            if (item.price > -1) {
                data.packages.push({package: (item.package as IPackage)._id, price: item.price});
            }
        });
        data.items = [];
        this.itemsList.forEach(item => {
            if (item.price && item.price > 0) {
                data.items.push({
                    item: item.item._id,
                    price: item.price,
                    discount: item.discount
                });
            }
        });

        this.storeService.updateStore(data).then((store: IStore) => {
            this.router.navigateByUrl('/apps/stores');
        });
    }

    addDelivery(city, price) {
        console.log(city, price);
        if (this.store.delivery === undefined) {
            this.store.delivery = [];
        }

        if (this.selectedCity !== undefined && this.selectedPrice >= 0) {
            const index = _.findIndex(this.store.delivery, (delivery) => delivery.city === this.selectedCity._id);
            if (index === -1) {
                this.store.delivery.push({city: this.selectedCity._id, price: this.selectedPrice});
                this.selectedPrice = 0;
                this.selectedCity = null;
            } else {
                this.store.delivery.splice(index, 1);
                this.store.delivery.push({city: this.selectedCity._id, price: this.selectedPrice});
                this.selectedPrice = 0;
                this.selectedCity = null;
            }
        }
    }

    editDelivery(delivery) {
        this.selectedCity = this.cities.find(city => delivery.city === city._id);
        this.selectedPrice = delivery.price;
    }

    getCityName(id): String {
        let city = _.find(this.cities, (city) => city._id === id);

        return city ? city.name.heb : 'undefined';
    }

    removeDelivery(city): void {
        let index = _.findIndex(this.store.delivery, (delivery) => delivery.city === city);
        if (index > -1) {
            this.store.delivery.splice(index, 1);
        }
    }

    removePhoto(image): void {
        let index: number = -1;
        index = this.store.pic.indexOf(image);
        if (index > -1) {
            this.store.pic.splice(index, 1);
        }
    }

    addProduct() {
        /*const data = this.productForm.getRawValue();
         data.handle = FuseUtils.handleize(data.name);
         this.productService.addProduct(data)
           .then(() => {

             // Trigger the subscription with new data
             this.productService.onProductChanged.next(data);

             // Show the success message
             this.snackBar.open('Product added', 'OK', {
               verticalPosition: 'top',
               duration: 2000
             });

             // Change the location with new one
             this.location.go('apps/e-commerce/products/' + this.product.id + '/' + this.product.handle);
           });*/
    }

    toggleDisabled(): void {
        this.disabled = !this.disabled;
    }

    addReview() {
        this.editedReview = new Rate({});
        this.reviewDialogRef = this.dialog.open(FuseReviewFormDialogComponent, {
            panelClass: 'contact-form-dialog',
            data: {
                action: 'add',
                review: this.editedReview
            }
        });
        this.reviewDialogRef.afterClosed().subscribe((response: FormGroup) => {
            if (!response) {
                return;
            }
            delete this.editedReview._id;
            this.editedReview.name = response.value.name;
            this.editedReview.rate = response.value.rate;
            this.editedReview.review = response.value.review;
            this.editedReview.disabled = response.value.disabled;
            this.store.rate.push(this.editedReview);
            this.storeService.updateStore(this.store).then(data => {
                console.log(this.store.rate);
            });

        });
    }

    editReview(review) {
        this.editedReview = review;
        this.reviewDialogRef = this.dialog.open(FuseReviewFormDialogComponent, {
            panelClass: 'contact-form-dialog',
            data: {
                action: 'edit',
                review: review
            }
        });
        this.reviewDialogRef.afterClosed().subscribe((response: FormGroup) => {
            if (!response) {
                return;
            }
            if (response[0] === 'save') {
                response = response[1];
                this.editedReview.name = response.value.name;
                this.editedReview.rate = response.value.rate;
                this.editedReview.review = response.value.review;
                this.editedReview.disabled = response.value.disabled;
                for (let i = 0; i < this.store.rate.length; i++) {
                    if (this.store.rate[i]._id === this.editedReview._id) {
                        this.store.rate[i] = this.editedReview;
                    }
                }
                this.storeService.updateStore(this.store).then(data => {
                    console.log(this.store.rate);
                });
            } else if (response[0] === 'delete') {
                this.editedReview.disabled = true;
                for (let i = 0; i < this.store.rate.length; i++) {
                    if (this.store.rate[i]._id === this.editedReview._id) {
                        this.store.rate[i] = this.editedReview;
                    }
                }
                this.storeService.updateStore(this.store).then(data => {
                    console.log(this.store.rate);
                });
            }
        });
    }
}
