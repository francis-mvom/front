import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id       : 'applications',
        title    : 'Applications',
        translate: 'NAV.APPLICATIONS',
        type     : 'group',
        children : [
            /*{
                id       : 'sample',
                title    : 'Sample',
                translate: 'NAV.SAMPLE.TITLE',
                type     : 'item',
                icon     : 'email',
                url      : '/sample',
                badge    : {
                    title    : '25',
                    translate: 'NAV.SAMPLE.BADGE',
                    bg       : '#F44336',
                    fg       : '#FFFFFF'
                }
            },*/
            {
                id       : 'contacts',
                title    : 'Contacts',
                translate: 'NAV.CONTACTS',
                type     : 'item',
                icon     : 'account_box',
                url      : '/apps/contacts'
            },
            {
                id       : 'city',
                title    : 'City',
                translate: 'NAV.CITY',
                type     : 'item',
                icon     : 'location_city',
                url      : '/apps/city'
            },
            {
                id       : 'item',
                title    : 'Item',
                translate: 'NAV.ITEM',
                type     : 'item',
                icon     : 'local_florist',
                url      : '/apps/items'
            },
            {
                id       : 'package',
                title    : 'Package',
                translate: 'NAV.PACKAGE',
                type     : 'item',
                icon     : 'fastfood',
                url      : '/apps/package'
            },
            {
                id       : 'store',
                title    : 'Stores',
                translate: 'NAV.STORE',
                type     : 'item',
                icon     : 'store',
                url      : '/apps/stores'
            },
            {
                id       : 'order',
                title    : 'Orders',
                translate: 'NAV.ORDER',
                type     : 'item',
                icon     : 'attach_money',
                url      : '/apps/orders'
            },
            {
                id       : 'articles',
                title    : 'Articles',
                translate: 'NAV.ARTICLE',
                type     : 'item',
                icon     : 'textsms',
                url      : '/apps/articles'
            },
            {
                id       : 'messages',
                title    : 'Message',
                translate: 'NAV.MESSAGE',
                type     : 'item',
                icon     : 'textsms',
                url      : '/apps/messages'
            },
        ]
    }
];
