import { Injectable, Injector } from '@angular/core';
import {
    HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import {AuthenticationService} from './authentication.service';
import {Router} from '@angular/router';
import {throwError} from 'rxjs';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    private authService: AuthenticationService;
    constructor(private injector: Injector) {}
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.authService = this.injector.get(AuthenticationService);
        const token: string = this.authService.getToken();
        request = request.clone({
            setHeaders: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            }
        });
        return next.handle(request);
    }

}
@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private router: Router) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        return next.handle(request)
            .catch((response: any) => {
                if (response instanceof HttpErrorResponse && response.status === 401) {
                    console.log(response);
                    localStorage.removeItem('token');
                    localStorage.removeItem('user');
                    this.router.navigateByUrl('/login');
                }
                return throwError(response);

            });
    }
}