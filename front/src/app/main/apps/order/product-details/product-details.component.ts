import { Component, OnInit } from '@angular/core';
import {OrderModuleService} from '../order.module.service';
import {ActivatedRoute, ActivatedRouteSnapshot} from '@angular/router';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Store} from '../../../../bussiness/model/store.model';
import {Package} from '../../../../bussiness/model/package.model';
import {Subject} from 'rxjs';
import {Item} from '../../../../bussiness/model/item.model';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {
    product : Item ;
    orderId : string;
    apiHost: string = environment.apihost;
    productForm: FormGroup;
    constructor(
        private _ordersModuleService: OrderModuleService,
        private route: ActivatedRoute,
        private _formBuilder: FormBuilder
    )
    {
        // Set the defaults
        const data = this.route.snapshot.data.order ;
        this.product = new Item(data.product);
        this.orderId = data.orderId;


        /*this.order.productOffer.store = new Store(this.order.productOffer.store);*/
        // Set the private defaults
       // this._unsubscribeAll = new Subject();
    }

  ngOnInit() {
      this.productForm = this.createProductForm();
  }
    createProductForm(): FormGroup
    {
        return this._formBuilder.group({
            id              : [this.product._id],
            name            : [this.product.name.en],
            rate            : [this.product.rate],
            description     : [this.product.description.en],
            categories      : [this.product.category],
            priceTaxIncl    : [this.product.price],
            priceTaxExcl    : [this.product.price],
            code            : [this.product.code],
            dayDeal         : [this.product.dayDeal],
            taxRate         : ['this.product.taxRate'],
            comparedPrice   : ['this.product.comparedPrice'],
            quantity        : ['this.product.quantity'],
            sku             : ['this.product.sku'],
            width           : ['this.product.width'],
            height          : ['this.product.height'],
            depth           : ['this.product.depth'],
            weight          : ['this.product.weight'],
            extraShippingFee: ['this.product.extraShippingFee'],
            active          : ['this.product.active'],
        });
    }

}
