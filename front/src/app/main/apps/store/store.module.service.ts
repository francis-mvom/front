import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import {
    ActivatedRouteSnapshot,
    Resolve,
    RouterStateSnapshot
} from "@angular/router";
import { BehaviorSubject, Observable, Subject } from "rxjs";

import { FuseUtils } from "@fuse/utils";

import { Store } from "app/bussiness/model/store.model";
import { StoreService } from "app/bussiness/services/store.service";
import {Message} from '../../../bussiness/model/message.model';

@Injectable()
export class StoreModuleService implements Resolve<any> {
    onStoresChanged: BehaviorSubject<any>;
    onSelectedStoresChanged: BehaviorSubject<any>;
    onSearchTextChanged: Subject<any>;
    onFilterChanged: Subject<any>;

    stores: Store[];
    selectedStores: string[] = [];

    searchText: string;
    filterBy: Array<any>;

    /**
     * Constructor
     *
     * @param {StoreService} storeService
     */
    constructor(private storeService: StoreService) {
        // Set the defaults
        this.onStoresChanged = new BehaviorSubject([]);
        this.onSelectedStoresChanged = new BehaviorSubject([]);
        this.onSearchTextChanged = new Subject();
        this.onFilterChanged = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {
            Promise.all([this.getStores()]).then(([files]) => {
                this.onSearchTextChanged.subscribe(searchText => {
                    this.searchText = searchText;
                    this.getStores();
                });

                this.onFilterChanged.subscribe(filter => {
                    this.filterBy = filter;
                    //this.getStores();
                });

                resolve();
            }, reject);
        });
    }

    /**
     * Get contacts
     *
     * @returns {Promise<any>}
     */
    getStores(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.storeService.list().subscribe(data => {
                this.stores = data;

                if (this.searchText && this.searchText !== "") {
                    this.stores = FuseUtils.filterArrayByString(
                        this.stores,
                        this.searchText
                    );
                }

                this.stores = this.stores.map(store => {
                    return new Store(store);
                });

                this.onStoresChanged.next(this.stores);
                resolve(this.stores);
            }, reject);
        });
    }

    /**
     * Toggle selected contact by id
     *
     * @param id
     */
    toggleSelectedStore(id): void {
        // First, check if we already have that contact as selected...
        if (this.selectedStores.length > 0) {
            const index = this.selectedStores.indexOf(id);

            if (index !== -1) {
                this.selectedStores.splice(index, 1);

                // Trigger the next event
                this.onSelectedStoresChanged.next(this.selectedStores);

                // Return
                return;
            }
        }

        // If we don't have it, push as selected
        this.selectedStores.push(id);

        // Trigger the next event
        this.onSelectedStoresChanged.next(this.selectedStores);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll(): void {
        if (this.selectedStores.length > 0) {
            this.deselectStores();
        } else {
            this.selectStores();
        }
    }

    /**
     * Select contacts
     *
     * @param filterParameter
     * @param filterValue
     */
    selectStores(filterParameter?, filterValue?): void {
        this.selectedStores = [];

        // If there is no filter, select all contacts
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedStores = [];
            this.stores.map(store => {
                this.selectedStores.push(store._id);
            });
        }

        // Trigger the next event
        this.onSelectedStoresChanged.next(this.selectedStores);
    }

    /**
     * Update contact
     *
     * @param contact
     * @returns {Promise<any>}
     */
    updateStore(id, store): Promise<any> {
        if(id === null){
            return new Promise((resolve, reject) => {
                this.storeService.create(store)
                    .subscribe(response => {
                        this.getStores();
                        resolve(response);
                    });
            });
        }else{
            return new Promise((resolve, reject) => {
                this.storeService.update(store)
                    .subscribe(response => {
                        this.getStores();
                        resolve(response);
                    });
            });
        }
    }

    /**
     * Deselect contacts
     */
    deselectStores(): void {
        this.selectedStores = [];

        // Trigger the next event
        this.onSelectedStoresChanged.next(this.selectedStores);
    }

    /**
     * Delete contact
     *
     * @param contact
     */
    deleteStore(store): void {
        const storeIndex = this.stores.indexOf(store);
        this.stores.splice(storeIndex, 1);
        this.onStoresChanged.next(this.stores);
    }

    /**
     * Delete selected contacts
     */
    deleteSelectedStores(): void {
        for (const storeId of this.selectedStores) {
            const store = this.stores.find(_store => {
                return _store._id === storeId;
            });
            const storeIndex = this.stores.indexOf(store);
            this.stores.splice(storeIndex, 1);
        }
        this.onStoresChanged.next(this.stores);
        this.deselectStores();
    }

    /**
     * sort store by date
     */
    getStoresBetweenDate(from: any, to: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.storeService.getBetweenDate(from, to).subscribe(data => {
                this.stores = data.map(item => {
                    return new Store(item);
                });
                this.onStoresChanged.next(this.stores);
                resolve(data);
            });
        });
    }
}
