import {DestinationType} from './Enum/DestinationType';
import {ICity} from './interfaces/ICity';
import {ICountry} from './interfaces/ICountry';

export class DelivertyAddress {
    city: ICity;
    street: string;
    number: string;
    floor: string;
    house : string ;
    country: ICountry | string;
    apartment: string;
    destinationType: DestinationType;
    companyName: string;
    instructions: string;
}
