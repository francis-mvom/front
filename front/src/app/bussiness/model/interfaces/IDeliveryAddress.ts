import {DestinationType} from '../Enum/DestinationType';
import {ICity} from './ICity';
import {ICountry} from './ICountry';

export interface IDeliveryAddress {
    destinationType: DestinationType;
    city: ICity;
    street: string;
    house: string;
    floor: number;
    country: ICountry | string;
    appartament: number;
    instructions: string;
}