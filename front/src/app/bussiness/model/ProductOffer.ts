import {IStore} from './interfaces/IStore';
import {ICity} from './interfaces/ICity';
import {IPackage} from './interfaces/IPackage';

export interface ProductOffer {
    store: IStore ;
    city: ICity | String;
    package: IPackage | String;
    finished: boolean;
}
