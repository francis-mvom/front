import { Injectable } from "@angular/core";
import {
    ActivatedRouteSnapshot,
    Resolve,
    RouterStateSnapshot
} from "@angular/router";
import { BehaviorSubject, Observable, Subject } from "rxjs";
import { environment } from "../../../../environments/environment";
import { FuseUtils } from "@fuse/utils";

import { Package } from "app/bussiness/model/package.model";
import { Item } from "app/bussiness/model/item.model";
import { PackageService } from "app/bussiness/services/package.service";
import { ItemService } from "app/bussiness/services/item.service";

@Injectable()
export class PackageModuleService implements Resolve<any> {
    onPackageChanged: BehaviorSubject<any>;
    onSelectedPackagesChanged: BehaviorSubject<any>;
    onItemDataChanged: BehaviorSubject<any>;
    onSearchTextChanged: Subject<any>;
    onFilterChanged: Subject<any>;

    packages: Array<Package>;
    items: Array<Item>;
    selectedPackages: string[] = [];

    searchText: string;
    filterBy: string;

    /**
     * Constructor
     *
     * @param {PackageService} _packageService
     * @param {ItemService} _itemService
     */
    constructor(
        private _packageService: PackageService,
        private _itemService: ItemService
    ) {
        // Set the defaults
        this.onPackageChanged = new BehaviorSubject([]);
        this.onItemDataChanged = new BehaviorSubject([]);
        this.onSelectedPackagesChanged = new BehaviorSubject([]);
        this.onSearchTextChanged = new Subject();
        this.onFilterChanged = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {
            Promise.all([this.getPackages(), this.getItemData()]).then(
                ([files]) => {
                    this.onSearchTextChanged.subscribe(searchText => {
                        this.searchText = searchText;
                        this.getPackages();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getPackages();
                    });

                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get packages
     *
     * @returns {Promise<any>}
     */
    getPackages(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._packageService.list().subscribe(response => {
                this.packages = response;

                /*if ( this.filterBy === 'starred' )
                        {
                            this.packages = this.packages.filter(_contact => {
                                return this.user.starred.includes(_contact._id);
                            });
                        }

                        if ( this.filterBy === 'frequent' )
                        {
                            this.packages = this.packages.filter(_contact => {
                                return this.user.frequentContacts.includes(_contact._id);
                            });
                        }*/

                if (this.searchText && this.searchText !== "") {
                    this.packages = FuseUtils.filterArrayByString(
                        this.packages,
                        this.searchText
                    );
                }

                this.packages = this.packages.map(p => {
                    return new Package(p);
                }); 

                this.onPackageChanged.next(this.packages);
                resolve(this.packages);
            }, reject);
        });
    } 

    /**
     * Get user data
     *
     * @returns {Promise<any>}
     */
    getItemData(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._itemService.list().subscribe((response: any) => {
                this.items = response;
                this.onItemDataChanged.next(this.items);
                resolve(this.items);
            }, reject);
        });
    }

    /**
     * Toggle selected contact by id
     *
     * @param id
     */
    toggleSelectedPackages(id): void {
        // First, check if we already have that contact as selected...
        if (this.selectedPackages.length > 0) {
            const index = this.selectedPackages.indexOf(id);
            if (index !== -1) {
                this.selectedPackages.splice(index, 1);
                // Trigger the next event
                this.onSelectedPackagesChanged.next(this.selectedPackages);
                // Return
                return;
            }
        }
        // If we don't have it, push as selected
        this.selectedPackages.push(id);
        // Trigger the next event
        this.onSelectedPackagesChanged.next(this.selectedPackages);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll(): void {
        if (this.selectedPackages.length > 0) {
            this.deselectPackages();
        } else {
            this.selectPackages();
        }
    }

    /**
     * Select packages
     *
     * @param filterParameter
     * @param filterValue
     */
    selectPackages(filterParameter?, filterValue?): void {
        this.selectedPackages = [];

        // If there is no filter, select all packages
        if (filterParameter === undefined || filterValue === undefined) {
            this.selectedPackages = [];
            this.packages.map(pack => {
                this.selectedPackages.push(pack._id);
            });
        }

        // Trigger the next event
        this.onSelectedPackagesChanged.next(this.selectedPackages);
    }

    /**
     * Update package
     *
     * @param package
     * @returns {Promise<any>}
     */
    updatePackage(id, pack): Promise<any> {
        if (id === null) {
            return new Promise((resolve, reject) => {
                this._packageService.create(pack).subscribe(response => {
                    this.getPackages();
                    resolve(response);
                });
            });
        } else {
            return new Promise((resolve, reject) => {
                this._packageService.update(pack).subscribe(response => {
                    this.getPackages();
                    resolve(response);
                });
            });
        }
    }

    /**
     * Update user data
     *
     * @param userData
     * @returns {Promise<any>}
     */
    /*updateUserData(userData): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient
                .post("api/contacts-user/" + this.user.id, { ...userData })
                .subscribe(response => {
                    this.getUserData();
                    this.getContacts();
                    resolve(response);
                });
        });
    }*/

    /**
     * Deselect packages
     */
    deselectPackages(): void {
        this.selectedPackages = [];

        // Trigger the next event
        this.onSelectedPackagesChanged.next(this.selectedPackages);
    }

    /**
     * Delete contact
     *
     * @param contact
     */
    deletePackage(pack): void {
        this._packageService.delete(pack._id).subscribe(data => {
            const contactIndex = this.packages.indexOf(pack);
            this.packages.splice(contactIndex, 1);
            this.onPackageChanged.next(this.packages);
        });
    }

    /**
     * Delete selected contacts
     */
    deleteSelectedPackages(): void {
        for (const packagetId of this.selectedPackages) {
            const pack = this.packages.find(_package => {
                return _package._id === packagetId;
            });
            const packageIndex = this.packages.indexOf(pack);
            this.packages.splice(packageIndex, 1);
            this._packageService.delete(pack._id).subscribe(data => {});
        }
        this.onPackageChanged.next(this.packages);
        this.deselectPackages();
    }
}
