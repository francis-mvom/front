import {Component, Inject, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {environment} from '../../../../../../environments/environment';
import {ItemModuleService} from '../../item.module.service';
import * as _ from 'lodash';
import {Item} from '../../../../../bussiness/model/item.model';
import {Category} from '../../../../../bussiness/model/category.model';
import {FileUploadService} from '../../../../../bussiness/services/file-upload.service';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import {  FileUploader } from 'ng2-file-upload';

@Component({
    selector: 'item-contact-form-dialog',
    templateUrl: './item-form.component.html',
    styleUrls: ['./item-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ItemsItemFormDialogComponent {
    editorConfig: AngularEditorConfig = {
        editable: true,
        spellcheck: true,
        height: '25rem',
        minHeight: '5rem',
        placeholder: 'Enter text here...',
        translate: 'no',
        uploadUrl: 'v1/images', // if needed
        customClasses: [ // optional
            {
                name: "quote",
                class: "quote",
            },
            {
                name: 'redText',
                class: 'redText'
            },
            {
                name: "titleText",
                class: "titleText",
                tag: "h1",
            },
        ]
    };
    action: string;
    item: Item;
    categories: Array<Category>;
    itemForm: FormGroup;
    dialogTitle: string;
    apiHost: string = environment.apihost;
    itemPicPath: string = '/assets/items/';
    picPath: string = environment.apihost + environment.uploadFile + environment.uploadItem;
    public uploader: FileUploader = new FileUploader({ url: environment.apihost + environment.uploadFile + environment.uploadItem, itemAlias: 'file' });
    /**
     * Constructor
     *
     * @param {MatDialogRef<CitiesCityFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<ItemsItemFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder,
    ) {

        // Set the defaults
        this.action = _data.action;
        this.categories = _data.categories;
        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Item';
            this.item = _data.item;
            this.categories = _data.categories;
        } else {
            this.dialogTitle = 'New Item';
            this.item = new Item({});
            this.categories = _data.categories;
        }

        /*this.fileUploadService.onPhotoUploaded.subscribe(data => {
            this.itemForm.get('picture').setValue(data);
        });*/
        this.itemForm = this.createContactForm();

        this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            // console.log('FileUpload:uploaded successfully:', item, status, response);
            this.itemForm.get('picture').setValue(JSON.parse(response).data[0].name);
            alert('Your file has been uploaded successfully');
        };
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create item form
     *
     * @returns {FormGroup}
     */
    createContactForm(): FormGroup {
        return this._formBuilder.group({
            _id: [this.item._id],
            name_en: [this.item.name.en],
            name_heb: [this.item.name.heb],
            description_en: [this.item.description.en],
            description_heb: [this.item.description.heb],
            code: [this.item.code],
            rate: [this.item.rate],
            picture: [this.item.picture],
            additional: [this.item.additional],
            recommended: [this.item.recommended],
            inStock: [this.item.inStock],
            category: [this.item.category],
            deal: [this.item.dayDeal],
            price: [this.item.price],
            seo: new FormControl(this.item.seo)
        });
    }
}
