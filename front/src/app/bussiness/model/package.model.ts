import {FuseUtils} from '@fuse/utils';
import {ITranslate} from './interfaces/ITranslate';
import { IPackage } from './interfaces/IPackage';
import {IItem} from './interfaces/IItem';
import {ItemSize} from './Enum/ItemSize';

export class Package implements IPackage {
    _id?: string;
    name: ITranslate;
    color: string;
    baseItem: IItem | string;
    size: ItemSize;
    addItems: Array<IItem> | Array<string>;
    price: number;
    /**
     * Constructor
     *
     * @param pack
     */
    constructor(pack: any) {
        {
            this._id = pack._id || '';
            this.name = pack.name || {heb: '', en: ''};
            this.color = pack.color || '#fff';
            this.baseItem = pack.baseItem || null;
            this.size = ItemSize.normal;
            this.addItems = pack.addItems || [];
            this.price = pack.price || 0;
        }
    }
}
