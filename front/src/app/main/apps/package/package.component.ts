import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {MatDialog} from '@angular/material';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';

import {PackageModuleService} from 'app/main/apps/package/package.module.service';
import {PackagesPackageFormDialogComponent} from 'app/main/apps/package/package-form/package-form.component';
import {Item} from 'app/bussiness/model/item.model';
import {Package} from '../../../bussiness/model/package.model';

@Component({
    selector: 'package',
    templateUrl: './package.component.html',
    styleUrls: ['./package.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class PackageComponent implements OnInit, OnDestroy {
    dialogRef: any;
    hasSelectedPackages: boolean;
    searchInput: FormControl;
    items: Array<Item> = [];
    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {PackageModuleService} _packageModuleService
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {MatDialog} _matDialog
     */
    constructor(
        private _packageModuleService: PackageModuleService,
        private _fuseSidebarService: FuseSidebarService,
        private _matDialog: MatDialog
    ) {
        // Set the defaults
        this.searchInput = new FormControl('');

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this._packageModuleService.onSelectedPackagesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(selectedPackages => {
                this.hasSelectedPackages = selectedPackages.length > 0;
            });

        this._packageModuleService.onItemDataChanged.subscribe(data => {
            this.items = data;
        });

        this.searchInput.valueChanges
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(300),
                distinctUntilChanged()
            )
            .subscribe(searchText => {
                this._packageModuleService.onSearchTextChanged.next(searchText);
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * New contact
     */
    newPackage(): void {
        this.dialogRef = this._matDialog.open(PackagesPackageFormDialogComponent, {
            panelClass: 'contact-form-dialog',
            data: {
                action: 'new',
                items: this.items
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                console.log(response);
                const newPackage = new Package({});
                newPackage.name.heb = response.value.name_heb;
                newPackage.name.en = response.value.name_en;
                newPackage.color = response.value.color;
                newPackage.baseItem = response.value.baseItem;
                newPackage.addItems = response.value.addItems;
                newPackage.size = response.value.size;
                newPackage.price = response.value.price;
                this._packageModuleService.updatePackage(null, newPackage);
            });
    }

    /**
     * Toggle the sidebar
     *
     * @param name
     */
    toggleSidebar(name): void {
        this._fuseSidebarService.getSidebar(name).toggleOpen();
    }
}
