import {Component, Inject, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

import {environment} from '../../../../../environments/environment';
import {Package} from '../../../../bussiness/model/package.model';
import {ItemSize} from '../../../../bussiness/model/Enum/ItemSize';
import {Item} from 'app/bussiness/model/item.model';

@Component({
    selector: 'packages-package-form-dialog',
    templateUrl: './package-form.component.html',
    styleUrls: ['./package-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class PackagesPackageFormDialogComponent {
    action: string;
    package: Package;
    packageForm: FormGroup;
    itemSize = ItemSize.values();
    itemsize = ItemSize;
    items: Array<Item> = [];
    baseItems: Array<Item> = [];
    additionalItems: Array<Item> = [];
    color: string = '#fff';
    dialogTitle: string;
    apiHost: string = environment.apihost;

    /**
     * Constructor
     *
     * @param {MatDialogRef<ContactsContactFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<PackagesPackageFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder
    ) {
        // Set the defaults
        this.action = _data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Package';
            this.package = _data.package;
            this.items = _data.items
        } else {
            this.dialogTitle = 'New Package';
            this.package = new Package({});
            this.items = _data.items
        }
        this.baseItems = this.items.filter(item => item.additional === false);
        this.additionalItems = this.items.filter(item => item.additional === true);
        this.packageForm = this.createPackageForm();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create contact form
     *
     * @returns {FormGroup}
     */
    createPackageForm(): FormGroup {
        return this._formBuilder.group({
            _id: [this.package._id],
            name_heb: [this.package.name.heb],
            name_en: [this.package.name.en],
            color: [this.package.color],
            size: [this.package.size],
            price: [this.package.price],
            baseItem: [this.package.baseItem],
            addItems: [this.package.addItems],
        });
    }

    setColor($event){
        this.packageForm.get('color').setValue($event);
    }
}
