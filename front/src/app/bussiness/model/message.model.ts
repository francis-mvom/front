import { ITranslate } from './interfaces/ITranslate';
import { IUser } from './interfaces/IUser';
import { MessageType } from './Enum/MessageType';

export class Message {
    _id: string ;
    title: string;
    text: string;
    type: MessageType | number ;
    theme : string;
    name : string ;
    email : string ;
    phone : string ;
    new : boolean
    from: IUser | string;
    to: IUser | string;
    createdAt : Date | string;
    updatedAt : Date | string;

    /**
     * Constructor
     *
     * @param message
     */
    public constructor(message) {
        {   
            this._id = message._id || '' ; 
            this.title = message.title || '' ;
            this.text = message.text || '';
            this.type = message.type;
            this.theme = message.theme || '' ;
            this.name = message.name || '';
            this.email = message.email || '' ;
            this.phone = message.phone || '' ;
            this.new = message.new;
            this.from = message.from || '' ;
            this.to = message.to || '' ;
            this.createdAt = message.createdAt || '' ;
            this.updatedAt = message.updatedAt || '' ;
        }
    }
}
