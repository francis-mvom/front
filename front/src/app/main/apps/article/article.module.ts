import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {
    MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule, MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatTableModule, MatToolbarModule,
    MatSelectModule, MatSortModule
} from '@angular/material';

import {TranslateModule} from '@ngx-translate/core';

import {FuseSharedModule} from '../../../../@fuse/shared.module';

import {ArticleComponent} from '../article/article.component';
import {ArticleModuleService} from './article.module.service';
import {ArticleService} from '../../../bussiness/services/article.service';
import {ArticleListComponent} from './article-list/article-list.component';
import {ArticleFormDialogComponent} from './article-form/article-form.component';
import {FuseConfirmDialogModule} from '../../../../@fuse/components';
import {HashveModule} from '../../../components/hashve.module';
import {AngularEditorModule} from '@kolkov/angular-editor';

const routes: Routes = [
    {
        path: '**',
        component: ArticleComponent,
        resolve: {
            articles: ArticleModuleService
        }
    }
];

@NgModule({
    declarations: [ArticleComponent, ArticleListComponent, ArticleFormDialogComponent],
    imports: [
        CommonModule,
        AngularEditorModule,
        RouterModule.forChild(routes),
        HashveModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatTableModule,
        MatToolbarModule,
        MatSelectModule,
        MatSortModule,
        TranslateModule,
        FuseConfirmDialogModule,
        FuseSharedModule,
    ],
    providers: [
        ArticleService,
        ArticleModuleService
    ],
    exports: [
        ArticleComponent
    ],
    entryComponents: [
        ArticleFormDialogComponent
    ]
})
export class ArticleModule {
}
