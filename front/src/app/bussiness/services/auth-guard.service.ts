import { Injectable } from '@angular/core';
import {Router, CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree} from '@angular/router';

import {AuthenticationService} from './authentication.service';
import {Observable} from 'rxjs';


@Injectable()
export class AuthGuardService implements CanActivate,CanActivateChild {
    constructor(
        public auth: AuthenticationService,
        public router: Router
    ) {}
    canActivate(): boolean {
        if (!this.auth.getToken()) {
            this.router.navigateByUrl('/');
            return false;
        }
        return true;
    }


    canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

        return this.canActivate();
    }
}