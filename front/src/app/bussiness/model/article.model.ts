import {ITranslate} from './interfaces/ITranslate';
import {IArticle} from './interfaces/IArticle';
import {ArticleType} from './Enum/ArticleType';
import {BindingType} from './Enum/BindingType';
import {Seo} from './seo';
import {ICity} from './interfaces/ICity';
import {IRegion} from './interfaces/IRegion';

export class Article implements IArticle {
    _id?: string;
    header: ITranslate;
    slogan: ITranslate;
    shitFromLeft: ITranslate;
    text: ITranslate;
    pic: String;
    showInList: boolean;
    showOnHomePage: boolean;
    articleType: ArticleType;
    binding: {
        bindingType: BindingType,
        object: String | ICity | IRegion;
    };
    seo: Seo;
    /**
     * Constructor
     *
     * @param item
     */
    constructor(item) {
        {
            if(item._id){
                this._id = item._id || '';
            }
            this.header = item.header || {heb: '', en: ''};
            this.slogan = item.slogan || {heb: '', en: ''};
            this.shitFromLeft = item.shitFromLeft || {heb: '', en: ''};
            this.text = item.text || {heb: '', en: ''};
            this.pic = (item.pic === '' || item.pic === undefined) ? 'assets/avatar/profile.jpg' : item.pic;
            this.showInList = item.showInList || false;
            this.showOnHomePage = item.showOnHomePage || false;
            this.articleType = item.articleType || '';
            if(item.binding){
                this.binding = {bindingType: item.binding.bindingType, object: item.binding.object};
            }else{
                this.binding = {bindingType: null, object: null};
            }
            this.seo = item.seo;
        }
    }
}
